﻿#pragma once
#include <map>
#include <vector>
#include "enum.h"
using namespace std;


class StateMessage
{
public:
	/// <summary>
	/// 添加某种状态，持续frame帧，默认永久持续
	/// </summary>
	void AddState(State state, int frame= (1 << 30) - 1);
	/// <summary>
	/// 是否存在某种状态
	/// </summary>
	bool ExistState(State state);
	/// <summary>
	/// 获取所有状态
	/// </summary>
	vector<State> GetAllState();
	/// <summary>
	/// 清除某种状态
	/// </summary>
	void ClearState(State state);
	/// <summary>
	/// 清除全部状态
	/// </summary>
	void ClearAllState();
private:
	/// <summary>
	/// 代表单位状态，key为剩余帧数
	/// </summary>
	map<int, int> UnitState;
};

