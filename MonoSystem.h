﻿#pragma once
#include <iostream>
#include <map>
#include <queue>
#include "MonoObject.h"
#include "SceneConfig.h"
#include <codecvt>
using namespace std;

using DrawingStyle = easy2d::DrawingStyle;


class MonoSystem : public Node
{
public:
	// 画面缩放时会影响的节点
	Node* RootNode = new Node();
	// 画面缩放时不影响这部分节点
	Node* UINode = new Node();
	// 对接easy2d
	void onUpdate();
	// 开启后使用红色标识真实的碰撞体,绿色标识设置为可穿越的碰撞体
	bool debugModel = true;
	// 是否暂停游戏
	bool Pause = false;
	static MonoSystem* GetInstance();
	/// <summary>
	/// 创建物体,flag为1时节点不受到画面缩放影响
	/// </summary>
	template <typename T>
	friend T* CreateObject(Utils::Rect body,int flag);
	/// <summary>
	/// 删除物体
	/// </summary>
	friend void DestroyObject(MonoObject* destroyObject);
	/// <summary>
	/// 初始化
	/// </summary>
	void Init();
	/// <summary>
	/// 帧主体
	/// </summary>
	void Run();
	/// <summary>
	/// 获取指定范围内可碰撞单位
	/// </summary>
	vector<MonoObject*> caculateCrash(Utils::Rect address);
	/// <summary>
	/// 获取场景中所有活跃物体
	/// </summary>
	vector<MonoObject*> getAllObjects();
	/// <summary>
	/// 新物体创建时调用
	/// </summary>
	vector<function<void(MonoObject*)>> onObjectCreate;
	/// <summary>
	/// 物体被销毁时调用
	/// </summary>
	vector<function<void(MonoObject*)>> onObjectDestroy;
private:
	MonoSystem() = default;
	int hashID=0;
	// 处理新增和删除的物体
	void DeleteObjectPoint();
	// 当前帧新增物体
	vector<MonoObject*> m_newObjects;
	// 等待删除的物体
	queue<MonoObject*> m_deletingObjects;
	// 当前关卡活跃的物体
	map<MonoObject*, bool> m_activeObjects;
	// 当前帧要进行移动的物体
	queue<MonoObject*> m_moveingObjects;
	// 发生碰撞的物体,元组第一个值为主体
	map < pair<MonoObject*, MonoObject*>,bool> m_crashObjects;
	void AddObject(MonoObject* obj);
	void DeleteObject(MonoObject* obj);
	void calculateMove();
	void noticeCrash();
};

template <typename T> 
static T* CreateObject(Utils::Rect body,int flag)
{
	//开始创建
	MonoObject* newObject = new T();
	// 设置相关参数
	newObject->setPos(body.posx,body.posy);
	newObject->setSize(body.width, body.height);
	newObject->hashId = ++MonoSystem::GetInstance()->hashID;
	// 新创建的物体执行创建函数
	newObject->onCreate();
	// 加入到管理器当中,用于easy2d的显示
	if (flag==0) MonoSystem::GetInstance()->RootNode->addChild(newObject);
	else MonoSystem::GetInstance()->UINode->addChild(newObject);
	// 管理起来并执行创建回调
	MonoSystem::GetInstance()->AddObject(newObject);
	// 判断是否开始debug模式
	if (MonoSystem::GetInstance()->debugModel)
	{
		//不可穿越物体绘制成红色，可穿越物体绘制成绿色
		auto Rect = ShapeNode::createRect(newObject->getSize());
		Rect->setDrawingMode(DrawingStyle::Mode::Round);
		if (!newObject->canThrough) Rect->setStrokeColor(Color::Red);
		else Rect->setStrokeColor(Color::Green);
		newObject->addChild(Rect);
	}
	// 输出召唤单位的名字
	std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
	wstring name = converter.from_bytes(typeid(T).name());
	cout << "创建: " << Utils::ws2s(name) << " hashID:" << newObject ->GetHashID()<< endl;
	// 动态转化
	return dynamic_cast<T*>(newObject);
}

inline static void DestroyObject(MonoObject* destroyObject)
{
	if (destroyObject)
		MonoSystem::GetInstance()->DeleteObject(destroyObject);
}
