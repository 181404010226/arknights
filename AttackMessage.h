﻿#pragma once
#include <iostream>
#include <functional>
#include "StateMessage.h"
#include "BattleObject.h"
#include "DamageMessage.h"
#include "enum.h"
class BattleObject;

class AttackMessage
{
public:
    /// <summary>
    /// 基础攻击间隔帧和攻击范围
    /// </summary>
    int AttackSpeed;
    /// <summary>
    /// 发动攻击到真正造成伤害的延迟(用于匹配动画)
    /// </summary>
    int AttackDelay=0;
    /// <summary>
    /// 范围要求
    /// </summary>
    RangeType rangeRequest;
    AttackType attackType;
    TargetType AttackTargets;
    vector<int> AttackRange;
    /// <summary>
    /// 获取下次攻击帧
    /// </summary>
    function<int (StateMessage*)> GetNextAttackFrame;
    /// <summary>
    /// 获取下次攻击目标
    /// </summary>
    function<vector<BattleObject*>(BattleObject*,StateMessage*)> GetAttackTargets;
    AttackMessage(AttackType type, TargetType targets, int speed, vector<int> range);
};

