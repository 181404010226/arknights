﻿#pragma once
#include "enum.h"

struct SummonMessage
{
	int x; 
	int y;
	/// <summary>
	/// 0~3,代表上下左右
	/// </summary>
	int direction;
	/// <summary>
	/// 0~1代表我方和对方
	/// </summary>
	int PlayerID;
	int SummonFrame;
	OperatorName operatorName;
};

