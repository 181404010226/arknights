﻿#pragma once
#pragma region 攻击相关
enum State
{
    unSelectable   // 无法选中状态
};

enum AttackType {
    NormalAttack //一半类型的攻击具有对称、且连续的特点 
};

enum RangeType {
    all,
    SamePlatform   // 只能攻击同平台的目标
};

// 目标敌人类型
enum TargetType {
    Enemy,
    AllEnemy,
    Friend,
    AllFriend,
    All
};

#pragma endregion
#pragma region 同步相关
enum MessageType {
	FrameSync,
	MySummonObject,
	OppoSummonObject,
	RetreatObject,
	ACK,
	Unknown
};
#pragma endregion
#pragma region 单位相关
enum class OperatorName {
    甜甜圈,
    基地,
    幽灵鲨
};

enum GridType {
    Special,
    Groud,
    Tower,
    Forbid
};

#pragma endregion
#pragma region 动画相关
enum class UnitAnimationState {
    Idle,
    Attack
};
#pragma endregion
#pragma region 窗口相关
// 表示当前窗口状态
enum WindowState {
    Waiting,
    Preparing,
    Playing
};

#pragma endregion
#pragma region 消息相关
enum PlayerMessageType {
    summonResultMessage,
    directionChooseMessage,
    gridClickedMessage
};
#pragma endregion





