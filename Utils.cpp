﻿#pragma once
#include"Utils.h"
#include<io.h>
#include <vector>
#include<iostream>
#include <string>
#include <codecvt>
#include <comutil.h>  
#include <algorithm>
#pragma comment(lib, "comsuppw.lib")
using namespace std;


namespace Utils {
    // 分割字符串
    vector<string> splitProtocol(string protocol,string delimiter) {
        vector<string> parts;
        size_t pos = 0;
        string token;
        while ((pos = protocol.find(delimiter)) != string::npos) {
            token = protocol.substr(0, pos);
            parts.push_back(token);
            protocol.erase(0, pos + delimiter.length());
        }
        parts.push_back(protocol);
        return parts;
    }
    // 计算镜像点
    void set_mirror_point(int& i, int& j, int n, int m) {
        i = n - 1 - i;
        j = m - 1 - j;
    }
    // 计算碰撞
    bool CrossLine(Rect r1, Rect r2)
    {

        float w = abs(r1.posx + r1.width / 2 - r2.posx - r2.width / 2);
        float h = abs(r1.posy + r1.height / 2 - r2.posy - r2.height / 2);

        if (w < (r1.width + r2.width) / 2 && h < (r1.height + r2.height) / 2)
            return true;
        else
            return false;
    }

    // 提取字符串中的数字
    int numberInStr(string temp, int startPos , int range )
    {
        int x = 0;
        if (range == 0) range = temp.length();
        for (int i = startPos; i < temp.length() && i < startPos + range; i++)
        {
            if (temp[i] >= '0' && temp[i] <= '9')
                x = x * 10 + temp[i] - '0';
        }
        return x;
    }
#pragma region 字符类型转换
    string ws2s(const wstring& ws)
    {
        _bstr_t t = ws.c_str();
        char* pchar = (char*)t;
        string result = pchar;
        return result;
    }

    wstring s2ws(const string& s)
    {
        _bstr_t t = s.c_str();
        wchar_t* pwchar = (wchar_t*)t;
        wstring result = pwchar;
        return result;
    }
    // 字符转换
    void Wchar_tToString(std::string& szDst, const wchar_t* wText)
    {
        DWORD dwNum = WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, NULL, 0, NULL, FALSE);//WideCharToMultiByte的运用
        char* psText;  // psText为char*的临时数组，作为赋值给std::string的中间变量
        psText = new char[dwNum];
        WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, psText, dwNum, NULL, FALSE);//WideCharToMultiByte的再次运用
        szDst = psText;// std::string赋值
        delete[]psText;// psText的清除
    }

    //不要忘记在使用完wchar_t*后delete[]释放内存
    wchar_t* multiByteToWideChar(const string& pKey)
    {
        auto pCStrKey = pKey.c_str();
        //第一次调用返回转换后的字符串长度，用于确认为wchar_t*开辟多大的内存空间
        int pSize = MultiByteToWideChar(CP_OEMCP, 0, pCStrKey, strlen(pCStrKey) + 1, NULL, 0);
        wchar_t* pWCStrKey = new wchar_t[pSize];
        //第二次调用将单字节字符串转换成双字节字符串
        MultiByteToWideChar(CP_OEMCP, 0, pCStrKey, strlen(pCStrKey) + 1, pWCStrKey, pSize);
        return pWCStrKey;
    }
#pragma endregion
}