﻿#pragma once
#include<io.h>
#include <vector>
#include<iostream>
#include <string>
#include <tuple>
using namespace std;


namespace Utils {
    const float EQS = 1e-6;
    struct Rect
    {
        float posx, posy;
        float width, height;        // Point 2
    };
    /// <summary>
    /// 计算镜像点
    /// </summary>
    void set_mirror_point(int& i, int& j, int n, int m);
    // 计算碰撞
    bool CrossLine(Rect r1, Rect r2);
    /// <summary>
    // 分割字符串
    /// </summary>
    vector<string> splitProtocol(string protocol, string delimiter);
    // 提取字符串中的数字
    int numberInStr(string temp, int startPos = 0, int range = 0);
#pragma region 字符类型转换
    string ws2s(const wstring& ws);

    wstring s2ws(const string& s);
    // 字符转换
    void Wchar_tToString(std::string& szDst, const wchar_t* wText);

    //不要忘记在使用完wchar_t*后delete[]释放内存
    wchar_t* multiByteToWideChar(const string& pKey);
#pragma endregion
}