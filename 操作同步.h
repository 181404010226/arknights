﻿#pragma once
#include "NetworkObject.h"
#include <queue>
#include <map>
#include <unordered_map>
#include "ExecuteMessage.h"
#include "enum.h"

using namespace std;
// 前向声明
class ExecuteMessage;

class 操作同步 :public NetworkObject
{
public:
	void onCreate();
	void onFrameUpdate();
	void GetNetworkMessage(string message);
private:
	// 记录对手帧率
	int OpponentFrame;
	// 代表帧数差距多少时触发同步
	void FrameSynchron();
	const int FrameLimit = 10;
	// 记录即将进行的操作
	queue<pair<int, string>> ACKmessage;
	// 由于操作发送方会不断发送，所有要记录索引
	unordered_map<int, bool> oppoACK;
	// 在第几帧要进行的操作
	map<int, ExecuteMessage* > operators;
};

