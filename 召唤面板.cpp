﻿#include "召唤面板.h"
#include "文本矩形.h"
#include "GridSystem.h"
#include "MonoSystem.h"
#include "幽灵鲨.h"
#include "PlayerSystem.h"
#include "SceneConfig.h"
#include <cmath>

template <typename T>
void 召唤面板::createSummonPanel(string picturePath,float deploymentTime,int cost)
{
    static int offset = 0;
#pragma region 创建头像与CP
    文本矩形* rect = CreateObject<文本矩形>({ 0,0,100, 100 });
    rect->setPos(rect->getPos() + Point((float)100 * offset, Window::getSize().height - 100));
    rect->SetPicture(picturePath+"/头像.png");
    // 加入白色背景
    auto backGroud = ShapeNode::createRect(Size{ 20,20 });
    backGroud->setPos(80, 0);
    backGroud->setFillColor(Color::White);
    backGroud->setOrder(25);
    rect->addChild(backGroud);
    // 右上角显示费用
    rect->text->setText(to_string(cost));
    rect->text->setPos(90,7);
    rect->text->setFontSize(15);
    rect->text->setFillColor(Color::Black);
    this->addChild(rect);
#pragma endregion
#pragma region 创建蒙版与冷却倒计时
    文本矩形* mask = CreateObject<文本矩形>({ 0,0,100, 100 });
    mask->setPos(mask->getPos() + Point((float)100 * offset, Window::getSize().height - 100));
    mask->setOpacity(0);
    mask->button->setFillColor(Color::Black);
    mask->text->setText("0");
    this->addChild(mask);
    masks.insert(make_pair(mask, deploymentTime));
#pragma endregion
    // 绑定加入房间的回调函数
    rect->OnClick = [this, mask,picturePath]() {
        // 冷却时间内不能点击
        if (stof(mask->text->getText()) > 0) return;
        if (GridSystem::GetInstance()->GetSummonObject() != nullptr)
            DestroyObject(GridSystem::GetInstance()->GetSummonObject());
        GridSystem::GetInstance()->SetSummonObject(CreateObject<T>());
        // 记录当前选择的UI
        clickedUI = mask;
        selectedObjectPath = picturePath;
    };
    offset++;
}

void 召唤面板::onCreate()
{
    createCPBar();
    sideBar = CreateObject<侧边栏>();
    addChild(sideBar);
    createSummonPanel<幽灵鲨>("./Image/幽灵鲨", 10.0f,14);
    createSummonPanel<幽灵鲨>("./Image/幽灵鲨", 20.0f, 14);
    createSummonPanel<幽灵鲨>("./Image/幽灵鲨", 70.0f, 14);
}

void 召唤面板::onFrameUpdate()
{
    CPBar->text->setText(to_string((int)PlayerSystem::GetInstance()->cost));
    PlayerSystem::GetInstance()->cost += CostPerSecond / Sceneconfig::GetInstance()->MaxFrame;
    // 更新CP条长度,模拟每秒读条的效果
    CPBar->button->setScaleX(fmodf(PlayerSystem::GetInstance()->cost, CostPerSecond)/ CostPerSecond);
    // 遍历蒙版，处理倒计时
    for (auto& it : masks)
    {
        float leaveTime = stof(it.first->text->getText());
        // 根据倒计时剩余量决定蒙版显示
        if (leaveTime > 0)
        {
            it.first->setOpacity(0.5);
            leaveTime -= 1.0f/Sceneconfig::GetInstance()->MaxFrame;
            it.first->text->setText(to_string(leaveTime).substr(0,5));
        }
        else
        {
            it.first->setOpacity(0);
        }
    }
}

void 召唤面板::OnGetUIMessage(UIBroadMessage message)
{
    // 玩家呼出/关闭朝向控制面板发送的消息
    if (message.type == gridClickedMessage)
    {
        if (message.Result == true)
        {
            // 显示侧边栏
            sideBar->SetPicturePath(selectedObjectPath);
            sideBar->SliderOut();
        }
        else sideBar->SliderIn();
    }
    // 在朝向控制选择完朝向之后会发送一个消息
    if (message.type==directionChooseMessage)
    {
        if (clickedUI)
        {
            maskCallbak[message.summonPos] = clickedUI;
            clickedUI->text->setText(to_string(masks[clickedUI]));
        }
    }
    if (message.type==summonResultMessage)
    {
        if (message.Result == false)
        {
            auto mask = maskCallbak[message.summonPos];
            mask->text->setText("0");
        }
    }
}

void 召唤面板::createCPBar()
{
    // CP背景条
    auto rect = ShapeNode::createRect({ 100, 10 });
    rect->setFillColor(Color::Black);
    // CP加载条
    CPBar = gcnew 文本矩形({ 100, 10 });
    CPBar->setPos(50, Window::getSize().height - 110);
    CPBar->button->setFillColor(Color::White);
    // CP数量
    CPBar->text->setPos(CPBar->text->getPos() + Point{ -30,-20 });
    // 挂载
    this->Node::addChild(CPBar);
}
