﻿#pragma once
#include "MonoObject.h"
#include "AttackMessage.h";
#include "DefenceMessage.h"
#include "StateMessage.h"
#include "DamageMessage.h"
#include "文本矩形.h"

class AttackMessage;
class DefenceMessage;
class DamageMessage;

class BattleObject :
    virtual public MonoObject
{
public:
    const int LifeBarWidth = 50;
    const int LifeBarHeight = 10;
    AttackMessage* attack = nullptr;
    DamageMessage* damage = nullptr;
    DefenceMessage* defense = nullptr;
    StateMessage* state = nullptr;
    /// <summary>
    /// 最大血量
    /// </summary>
    int Maxlife;
    /// <summary>
    /// 当前血量
    /// </summary>
    int Life;
    /// <summary>
    /// 战斗状态的帧更新
    /// </summary>
    void onBattleUpdate();
    void onCreate();
    ~BattleObject();
protected:
    // 暂时用来代替血条
    文本矩形* BloodBar = nullptr;
private:
    // 封装造成伤害的函数
    void MakeDamage();
    int NextAttackFrame=-1;
    // 真正造成伤害的帧
    int NextDamageFrame = -1;
};

