﻿#pragma once
#include <vector>
#include "WindowObject.h"
#include <set>
#include "enum.h"
using namespace std;
using Point = easy2d::Point;


class WindowSystem
{
public:
	WindowState windowState;
	/// <summary>
	/// 菜单界面
	/// </summary>
	void MenuWindow();
	/// <summary>
	/// 对战界面
	/// </summary>
	void PlayWindow();
	/// <summary>
	/// 获取当前对战了多少帧
	/// </summary>
	void NameWindow();
	/// <summary>
	/// 设置镜头中心
	/// </summary>
	void SetTargetPos(Point pos);
	int GetBattleFrameCnt();
	static WindowSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	~WindowSystem();
private:
	// 计算对战帧
	int battleFrameCnt = 0;
	// 作为各界面的根节点
	Node* root;
	void clearNoNeedObjects();
	void InitWindow(int width, int height, string picture,bool resize=true);
	WindowSystem() = default;
	// 存储所有物体
	set<WindowObject*> WindowObjects;
	// 镜头目标位置中心
	Point targetPos;
	// 镜头其实位置中心
	Point startPos;
	// 镜头当前移动的帧数
	float currentTime = 60;
};
