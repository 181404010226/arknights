﻿#include "AnimationObject.h"
#include "WindowSystem.h"
#include <fstream>

bool isFileExist(const std::string& fileName)
{
	std::ifstream infile(fileName.c_str());
	return infile.good();
}


void AnimationObject::onAnimationUpdate()
{
	if (!beginPlay) return;
	// 编号+1
	size_t lastNumPos = nowPlay.rfind(to_string(playingFrame)); // 找到路径中最后一个数字所在位置
	string numStr;
	if (lastNumPos != string::npos) { // 如果找到了数字
		numStr = nowPlay.substr(lastNumPos, to_string(playingFrame).length()); // 将数字部分截取下来
		// 每偶数帧++
		if (WindowSystem::GetInstance()->GetBattleFrameCnt() % 2 == 0)
			playingFrame++; // 帧数++
		string newNumStr = to_string(playingFrame); // 将新的数字转换成字符串形式
		nowPlay.replace(lastNumPos, numStr.size(), newNumStr); // 将原字符串中的数字替换为新的数字
	}
	// 不存在文件
	if (!isFileExist(nowPlay))
	{
		if (AutoToIdle)
		{
			AutoToIdle = false;
			ResetToState(UnitAnimationState::Idle);
		}
		else
		{
			nowPlay.replace(lastNumPos, numStr.size(), "1");
			playingFrame = 1;
		}
	}
	// 替换贴图
	unitStamp->SwitchPicture(nowPlay);
}

void AnimationObject::SetPreViewDirection(int number,int row)
{
	Node* spriteTemp = unitStamp->getChild("指示器");
	if (!spriteTemp)
	{
		auto directionMark = gcnew Sprite;
		// 从本地图片加载
		directionMark->open("Image/指示器.png");
		// 设置旋转轴
		directionMark->setAnchor(0.5, 0.5);
		// 绑定指示器
		unitStamp->addChild(directionMark);
		directionMark->setOrder(-1);
		directionMark->setName("指示器");
		spriteTemp = unitStamp->getChild("指示器");
	}
	// 设置位置
	unitStamp->pixel = unitStamp->defaultPixel + row * unitStamp->increasePixel;
	spriteTemp->setPos(unitStamp->pixel / 2, unitStamp->pixel / 4 * 3);
	spriteTemp->setSize({ unitStamp->pixel / 2,unitStamp->pixel / 2 });
	// 切换图片
	unitStamp->SwitchPicture(IdlePicture[number]);
	// 旋转图片
	switch (number)
	{
	case 0:
		spriteTemp->setRotation(0);
		break;
	case 1:
		spriteTemp->setRotation(180);
		break;
	case 2:
		spriteTemp->setRotation(270);
		break;
	case 3:
		spriteTemp->setRotation(90);
		break;
	}
	direction = number;
}

void AnimationObject::ResetToState(UnitAnimationState state,bool toIdle)
{
	beginPlay = true;
	playingFrame = 1;
	AutoToIdle = toIdle;
	switch (state)
	{
	case UnitAnimationState::Idle:
		nowPlay = IdlePicture[direction];
		break;
	case UnitAnimationState::Attack:
		nowPlay = AttackPicture[direction];
		break;
	default:
		break;
	}
}
