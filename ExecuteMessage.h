﻿#pragma once
#include <vector>
#include "enum.h"
using namespace std;


class ExecuteMessage
{
public:
    ExecuteMessage(MessageType type,vector<int> Message);
    int PlayerID=-1;
    int ACKid = -1;
    int FrameCnt = -1;
    int Posx = -1;
    int Posy = -1;
    int Direction = -1;
    int ObjectType = -1;
};

