﻿#include "PlayerSystem.h"
#include <fstream>
#include "MonoSystem.h"


string PlayerSystem::getPlayerName()
{
	return playerName;
}

PlayerSystem* PlayerSystem::GetInstance()
{
    // 饿汉式
    static PlayerSystem m_Instance;
    return &m_Instance;
}

void PlayerSystem::Run()
{
}

bool PlayerSystem::Init()
{
	return true;
}
void PlayerSystem::PreInit()
{
	// 注册回调函数
	MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
		PlayerObject* obj = dynamic_cast<PlayerObject*>(newObj);
		if (obj) PlayerObjects.insert(obj);
		});
	MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
		PlayerObject* obj = dynamic_cast<PlayerObject*>(newObj);
		if (obj) PlayerObjects.erase(obj);
		});
}
void PlayerSystem::setPlayerName(const std::string& name) {
    playerName = name;
}
PlayerSystem::~PlayerSystem()
{
}