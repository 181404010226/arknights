﻿#include "GridObject.h"
#include "AnimationObject.h"


void GridObject::SetDirection(int number)
{
	GetComponent<AnimationObject>(this)->SetPreViewDirection(number, get<0>(GridPos));
	m_direction = number;
}

int GridObject::GetDirection()
{
	return m_direction;
}

GridObject::~GridObject()
{
	cout << "网格部分清除" << endl;
}
