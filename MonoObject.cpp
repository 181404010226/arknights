﻿#include "MonoObject.h"


Utils::Rect MonoObject::GetBody()
{
	return Utils::Rect{ getPosX(),getPosY()  ,getSize().width,getSize().height};
}


int MonoObject::GetHashID()
{
	return hashId;
}


void MonoObject::onCreate()
{
}

void MonoObject::onCrash(MonoObject* collider)
{
}


void MonoObject::onFrameUpdate()
{

}

void MonoObject::onDestroy()
{

}


MonoObject::~MonoObject()
{
	cout << "基类部分清除 hashid: （为0代表未受管理）" <<  GetHashID() <<   endl;
}

void MonoObject::addChild(Node* child)
{
	this->Node::addChild(child);
}

void MonoObject::addChild(MonoObject* child)
{
	child->removeFromParent();
	this->Node::addChild(child);
}
