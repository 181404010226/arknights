﻿#include "AnimationSystem.h"
#include "MonoSystem.h"
#include <string>

AnimationSystem* AnimationSystem::GetInstance()
{
    // 饿汉式
    static AnimationSystem m_Instance;
    return &m_Instance;
}

bool AnimationSystem::Init()
{
   
	return true;
}

void AnimationSystem::PreInit()
{ 
    // 注册回调函数
    MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
        AnimationObject* obj = dynamic_cast<AnimationObject*>(newObj);
        if (obj) AnimationObjects.insert(obj);
        });
    MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
        AnimationObject* obj = dynamic_cast<AnimationObject*>(newObj);
        if (obj) AnimationObjects.erase(obj);
        });
}


void AnimationSystem::Run()
{
    for (auto it = AnimationObjects.begin(); it != AnimationObjects.end(); ++it)
    {
        (*it)->onAnimationUpdate();
    }
}


AnimationSystem::~AnimationSystem()
{
}

