﻿#pragma once
#include <iostream>
#include <functional>
#include "StateMessage.h"
#include "BattleObject.h"
using namespace std;

class BattleObject;

class DamageMessage
{
public:
    /// <summary>
    /// 伤害值
    /// </summary>
    int physicalDamage, magicDamage;
    /// <summary>
    /// 通用伤害改进函数
    /// </summary>
    function< DamageMessage* (BattleObject*)> commonModifierCallback;
    function<DamageMessage*(BattleObject*)> damageModifierCallback;
    function<void(DamageMessage*,BattleObject*)> damageCalculationCallback=nullptr;
    DamageMessage(int physical, int magic);
};

