﻿#include "WindowSystem.h"
#include "MonoSystem.h"
#include "NetWorkSystem.h"
#include "GridSystem.h"
#include "房间管理.h"
#include "PlayerObject.h"
#include "Debug.h"
#include "Keyboard.h"
#include "MonoObject.h"
#include "操作同步.h"
#include "召唤面板.h"
#include "PlayerSystem.h"
#include "AI编写.h"




// 生成窗口背景
void WindowSystem::InitWindow(int width, int height, string picture,bool resize)
{
	if (root->getChild("HOME")) root->getChild("HOME")->~Node();
	// easy2d
#pragma region 这里不能写GCNEW，否则关闭应用时会由于析构顺序导致报错
	auto img = new Sprite();
#pragma endregion
	bool check = img->open(picture);
	if (resize) img->setSize(width, height);
	root->addChild(img);
	Window::setSize(width, height);
}
void WindowSystem::NameWindow()
{
	CreateObject<Keyboard>();
	windowState = Waiting;
	InitWindow(640, 480, "Image/名字输入背景图.jpg");
	// 关闭网格监听
	GridSystem::GetInstance()->closeGridsListener();
}

void WindowSystem::MenuWindow()
{
	windowState = Waiting;
	clearNoNeedObjects();
	InitWindow(640, 480, "Image/开始界面背景.jpg");	
	CreateObject<房间管理>(Utils::Rect{
		Window::getWidth() / 2,Window::getHeight() - 100,100, 100 });
	// 关闭网格监听
	GridSystem::GetInstance()->closeGridsListener();
}

void WindowSystem::PlayWindow()
{
	windowState = Playing;
	clearNoNeedObjects();
	InitWindow(800, 600, "Image/对战界面.png",false);
	CreateObject<操作同步>();
	CreateObject<召唤面板>({0,0,0,0},1);
	// 重新唤起网格
	GridSystem::GetInstance()->reset();
	// 创建基地
	GridSystem::GetInstance()->Summon({ 2,0, 2,0,0, OperatorName::基地 });
	GridSystem::GetInstance()->Summon({ 2,6, 3,1,0, OperatorName::基地 });
	// 重置起始费用
	PlayerSystem::GetInstance()->cost = 0;
}

void WindowSystem::SetTargetPos(Point pos)
{
	startPos = MonoSystem::GetInstance()->RootNode->getPos();
	targetPos = pos;
	currentTime = 0;
}

int WindowSystem::GetBattleFrameCnt()
{
	return battleFrameCnt;
}

WindowSystem* WindowSystem::GetInstance()
{
	// 饿汉式
	static WindowSystem m_Instance;
	return &m_Instance;
}

void WindowSystem::Run()
{
	// 如果进入对战界面，每帧都需要传递信息
	if (windowState == Playing)
	{
		battleFrameCnt++;
		NetworkSystem::GetInstance()->AppendNetworkMessage("SYNC "+to_string(battleFrameCnt), true, false);
	}
	else battleFrameCnt = 0;
	// 平滑移动镜头
	if (currentTime <= 20)
	{
		float t = currentTime / 20;
		float currentX = Utils::lerp(startPos.x, targetPos.x, t);
		float currentY = Utils::lerp(startPos.y, targetPos.y, t);
		MonoSystem::GetInstance()->RootNode->setPos({ currentX ,currentY });
		// 增加当前时间
		currentTime += 1.0f;
	}
}

bool WindowSystem::Init()
{
	root = MonoSystem::GetInstance()->RootNode;
	CreateObject<Debug>();
	NameWindow();
	
	//MenuWindow();
	// PlayWindow();
	
	return true;
}

void WindowSystem::PreInit()
{
	// 注册回调函数
	/* MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
		WindowObject* obj = dynamic_cast<WindowObject*>(newObj);
		if (obj) WindowObjects.insert(obj);
		});
	MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
		WindowObject* obj = dynamic_cast<WindowObject*>(newObj);
		if (obj) WindowObjects.erase(obj);
		});*/
}

WindowSystem::~WindowSystem()
{
}

void WindowSystem::clearNoNeedObjects()
{
	vector<MonoObject*> objs = MonoSystem::GetInstance()->getAllObjects();
	for (int i = 0; i < objs.size(); i++)
	{
		// 不清除根节点
		if (objs[i]->type!=DonotDestroyed) DestroyObject(objs[i]);
	}
}