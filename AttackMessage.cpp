﻿#include "AttackMessage.h"
#include "WindowSystem.h"
#include "GridSystem.h"

AttackMessage::AttackMessage(AttackType type, TargetType targets, int speed, vector<int> range):
	attackType(type), AttackSpeed(speed), AttackRange(range), AttackTargets(targets)
{
	// 配置默认情况
	GetNextAttackFrame = [this](StateMessage* state) {
		return WindowSystem::GetInstance()->GetBattleFrameCnt() + AttackSpeed;
	};
	GetAttackTargets = [this](BattleObject* obj, StateMessage* state) {
		return GridSystem::GetInstance()->GetAttackRangeObjects(obj);
	};
}
