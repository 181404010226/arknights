﻿#include "StateMessage.h"
#include "WindowSystem.h"

void StateMessage::AddState(State state, int frame)
{
    UnitState[state] = WindowSystem::GetInstance()->GetBattleFrameCnt() + frame;
}

bool StateMessage::ExistState(State state)
{
    if (UnitState.find(state) != UnitState.end() &&
        UnitState[state] > WindowSystem::GetInstance()->GetBattleFrameCnt()) return true;
    return false;
}

vector<State> StateMessage::GetAllState()
{
    vector<State> result;
    for (auto& it : UnitState) {
        if (it.second > WindowSystem::GetInstance()->GetBattleFrameCnt()) {
            result.push_back(State(it.first));
        }
    }
    return result;
}

void StateMessage::ClearState(State state)
{
    UnitState[state] = 0;
}

void StateMessage::ClearAllState()
{
    UnitState.clear();
}
