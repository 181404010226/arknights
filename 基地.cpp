﻿#include "基地.h"

void 基地::onCreate()
{
	// 记得调用基类的创建函数(显示血条)
	BattleObject::onCreate();
	// 设置类型为地面类型
	gridType = Groud;
	// 设置显示层级
	type = playerUnit1;
	IdlePicture[0] = "Image/基地/我方基地1.png";
	IdlePicture[1] = "Image/基地/对方基地1.png";
	IdlePicture[2] = "Image/基地/我方基地1.png";
	IdlePicture[3] = "Image/基地/对方基地1.png";
	AttackPicture[0] = "Image/基地/我方基地1.png";
	AttackPicture[1] = "Image/基地/对方基地1.png";
	AttackPicture[2] = "Image/基地/我方基地1.png";
	AttackPicture[3] = "Image/基地/我方基地1.png";
	unitStamp = gcnew 网格单位贴图(IdlePicture[3]);
	this->addChild(unitStamp);
	// 设置生命
	Maxlife = 5000;
	Life = 5000;
	// 设置攻击属性
	attack = new AttackMessage(NormalAttack, AllEnemy, 9999, { 0,0 });
	// 设置伤害与防御属性
	damage = new DamageMessage(0, 0);
	defense = new DefenceMessage(0, 0);
	state = new StateMessage();
	// 设置干员名
	typeName = OperatorName::幽灵鲨;
}

void 基地::onFrameUpdate()
{
}

void 基地::onDestroy()
{
}
