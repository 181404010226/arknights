﻿#pragma once
#include "MonoObject.h"

// 作为各系统的根节点
class Debug :
    virtual public MonoObject
{
    void onCreate();
    void onFrameUpdate();
};

