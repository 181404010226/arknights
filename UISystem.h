﻿#pragma once
#include <vector>
#include "UIObject.h"
#include <set>
#include "enum.h"
using namespace std;

class UISystem
{
public:
	static UISystem* GetInstance();
	void Run(bool Pause);
	bool Init();
	void PreInit();
	/// <summary>
	/// 向其他UI广播消息
	/// </summary>
	void BroadMessage(UIBroadMessage message);
	~UISystem();
private:
	UISystem() = default;
	// 存储所有物体
	set<UIObject*> UIObjects;
};


