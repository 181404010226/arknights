﻿#include "Keyboard.h"
#include "WindowSystem.h"
#include "PlayerSystem.h"
using Font = easy2d::Font;
using KeyCode = easy2d::KeyCode;

void Keyboard::onCreate()
{
    // 创建一个字体，宋体、字号40、粗体、斜体
    text = gcnew Text("按键输入姓名(回车确认):\n\n");
    Font font = Font();
    font.family = "宋体";
    font.size = 50;
    font.weight = Font::Weight::Bold;
    font.italic = true;
    text->setFont(font);
    this->addChild(text);
}
void Keyboard::onFrameUpdate()
{
    string text1 = text->getText();
    if (Input::isPress(KeyCode::A)) text1 += "A";
    else if (Input::isPress(KeyCode::B)) text1 += "B";
    else if (Input::isPress(KeyCode::C)) text1 += "C";
    else if (Input::isPress(KeyCode::D)) text1 += "D";
    else if (Input::isPress(KeyCode::E)) text1 += "E";
    else if (Input::isPress(KeyCode::F)) text1 += "F";
    else if (Input::isPress(KeyCode::G)) text1 += "G";
    else if (Input::isPress(KeyCode::H)) text1 += "H";
    else if (Input::isPress(KeyCode::I)) text1 += "I";
    else if (Input::isPress(KeyCode::J)) text1 += "J";
    else if (Input::isPress(KeyCode::K)) text1 += "K";
    else if (Input::isPress(KeyCode::L)) text1 += "L";
    else if (Input::isPress(KeyCode::M)) text1 += "M";
    else if (Input::isPress(KeyCode::N)) text1 += "N";
    else if (Input::isPress(KeyCode::O)) text1 += "O";
    else if (Input::isPress(KeyCode::P)) text1 += "P";
    else if (Input::isPress(KeyCode::Q)) text1 += "Q";
    else if (Input::isPress(KeyCode::R)) text1 += "R";
    else if (Input::isPress(KeyCode::S)) text1 += "S";
    else if (Input::isPress(KeyCode::T)) text1 += "T";
    else if (Input::isPress(KeyCode::U)) text1 += "U";
    else if (Input::isPress(KeyCode::V)) text1 += "V";
    else if (Input::isPress(KeyCode::W)) text1 += "W";
    else if (Input::isPress(KeyCode::X)) text1 += "X";
    else if (Input::isPress(KeyCode::Y)) text1 += "Y";
    else if (Input::isPress(KeyCode::Z)) text1 += "Z";
    else if (Input::isPress(KeyCode::Num0)) text1 += "0";
    else if (Input::isPress(KeyCode::Num1)) text1 += "1";
    else if (Input::isPress(KeyCode::Num2)) text1 += "2";
    else if (Input::isPress(KeyCode::Num3)) text1 += "3";
    else if (Input::isPress(KeyCode::Num4)) text1 += "4";
    else if (Input::isPress(KeyCode::Num5)) text1 += "5";
    else if (Input::isPress(KeyCode::Num6)) text1 += "6";
    else if (Input::isPress(KeyCode::Num7)) text1 += "7";
    else if (Input::isPress(KeyCode::Num8)) text1 += "8";
    else if (Input::isPress(KeyCode::Num9)) text1 += "9";
    // 设置字体
    text->setText(text1);
    // 完成输入
    if (Input::isPress(KeyCode::Enter))
    {
        string temp = text->getText().substr(text->getText().find_last_of('\n')+1);
        if (temp.length() > 0)
        {
            NetworkSystem::GetInstance()->AppendNetworkMessage(temp, true);
            WindowSystem::GetInstance()->MenuWindow();
            PlayerSystem::GetInstance()->setPlayerName(temp);
        }
    }
}