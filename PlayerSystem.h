﻿#pragma once
#include <vector>
#include "PlayerObject.h"
#include "NetWorkSystem.h"
#include <set>
using namespace std;

class PlayerSystem
{
public:
	friend class NetworkSystem;
	/// <summary>
	/// 玩家当前持有费用
	/// </summary>
	float cost=0;
	/// <summary>
	/// 玩家操作数
	/// </summary>
	int ACKid = 0;
	/// <summary>
	/// 获取玩家名字
	/// </summary>
	string getPlayerName();
	static PlayerSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	void setPlayerName(const std::string& name);
	~PlayerSystem();
private:
	string playerName;
	PlayerSystem() = default;
	// 存储所有物体
	set<PlayerObject*> PlayerObjects;
};
