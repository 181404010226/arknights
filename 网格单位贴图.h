﻿#pragma once
#include <easy2d/easy2d.h>
using namespace std;
using namespace easy2d;
class 网格单位贴图 :public Node
{
public:
	const float defaultPixel = 100.0f;
	const float increasePixel = 2.0f;
	/// <summary>
	/// 贴图大小，根据位置调整
	/// </summary>
	float pixel = 110.0f;
	// 贴图
	Sprite* sprite=nullptr;
	/// <summary>
	/// 传入单位的上下左右四个面向的贴图
	/// </summary>
	网格单位贴图(string Picture);
	/// <summary>
	/// 切换网格单位的贴图
	/// </summary>
	void SwitchPicture(string picturePath);
};

