﻿#pragma once
#include "UIObject.h"
using Size = easy2d::Size;

class 文本矩形 : public UIObject
{
public:
	/// <summary>
	/// 响应区域
	/// </summary>
	ShapeNode* button;
	/// <summary>
	/// 图片
	/// </summary>
	Sprite* sprite;
	// 文本
	Text* text;
	/// <summary>
	/// 传入创建的位置和大小
	/// </summary>
	文本矩形(Size size);
	文本矩形();
	void onCreate();
	// 设置贴图
	void SetPicture(string picturePath);
private:
	void Init(Size size);
};
