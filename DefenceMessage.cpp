﻿#include "DefenceMessage.h"

DefenceMessage::DefenceMessage(int physical, int magic):
	physicalDefence(physical), magicDefence(magic)
{
	// 配置通用情况
	commonModifierCallback = [this](BattleObject* obj) {
		DefenceMessage* result = new DefenceMessage(*this);
		return result;
	};
	defenceModifierCallback= [this](BattleObject* obj) {
		return commonModifierCallback(obj);
	};
}
