﻿#pragma once
#include <vector>
#include <set>
#include "GridObject.h"
#include "文本矩形.h"
#include "BattleObject.h"
#include "SummonMessage.h"
#include "AttackMessage.h"
#include "朝向控制.h"
using namespace std;


class GridSystem
{
public:
	// 网格长宽
	const int BoardHeight=5;
	const int BoardWidth =7;
	/// <summary>
	/// 获取当前准备召唤的单位
	/// </summary>
	/// <returns></returns>
	GridObject* GetSummonObject();
	/// <summary>
	/// 设置当前准备召唤的单位
	/// </summary>
	void SetSummonObject(GridObject* obj);
	// 判断当前是不是选择朝向阶段
	bool isDirectPhase = false;
	static GridSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	/// <summary>
	/// 开始对局时要重置网格
	/// </summary>
	void reset();
	/// <summary>
	/// 在指定网格直接召唤干员
	/// </summary>
	GridObject* Summon(SummonMessage msg);
	// 获取攻击范围内的所有单位
	vector<BattleObject*> GetAttackRangeObjects(BattleObject* battleObj);
	/// <summary>
	/// 显示指定干员的攻击范围
	/// </summary>
	void ShowObjectAttackRange(BattleObject* battleObj);
	/// <summary>
	/// 取消攻击范围的显示
	/// </summary>
	void DisShowObjectAttackRange();
	/// <summary>
	/// 获取制定网格的单位
	/// </summary>
	GridObject* GetGridObejct(int i, int j);
	~GridSystem();
	// 关闭网格监听
	void closeGridsListener();
	// 开启网格监听
	void openGridsListener();
private:
	// 本系统挂载图片的节点
	Node* root;
	GridObject* summonObject = nullptr;
	// 控制summonObject的朝向
	朝向控制* control;
	GridSystem() = default;
	void setToGrid(GridObject* gridObject, int x, int y);
	// 响应点击
	Function<void()> responseMouseClick(int gridIndexi, int gridIndexj);
	// 注册网格事件
	void registerGirdEvent(ShapeNode* shapeNode,int gridIndexi, int gridIndexj);
	// 判断某个位置是否召唤成功，-1都失败，0失败，1成功
	int judgeSummonState(GridObject* obj,SummonMessage msg);
	// 获取特定攻击范围内的所有网格
	vector<tuple<int,int>> GetRangeInGrids(BattleObject* battleObj);
	// 存储所有物体
	set<GridObject*> AllGridObjects;
	// 存储所有网格
	vector<vector<文本矩形*>> shapeGrids;
	// 存储所有在网格中的物体
	vector<vector<GridObject*>> objectInGrids;
	// 地形类型配置
	vector<vector<GridType>> GridsType = { 
	{Groud,Groud,Groud,Groud,Groud,Groud,Groud},
	{Groud,Tower,Tower,Groud,Tower,Tower,Groud},
	{Groud,Groud,Groud,Groud,Groud,Groud,Groud},
	{Groud,Tower,Tower,Groud,Tower,Tower,Groud},
	{Groud,Groud,Groud,Groud,Groud,Groud,Groud},								  
	};
};
