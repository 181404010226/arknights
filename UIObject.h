﻿#pragma once
#include "MonoObject.h"
#include "UIBroadMessage.h"
using namespace std;

class UIObject :virtual public MonoObject
{
public:
	/// <summary>
	/// UI帧更新时调用,返回1代表触发了事件
	/// </summary>
	int onUIUpdate();
	ShapeNode* ButtonArea=nullptr;
	/// <summary>
	/// 被点击时调用
	/// </summary>
	function<void()> OnClick;
	/// <summary>
	/// 鼠标悬停时持续调用
	/// </summary>
	function<void()> OnMouseIn;
	/// <summary>
	/// UI接收消息广播(或者直接传递UIObject调用)
	/// </summary>
	virtual void OnGetUIMessage(UIBroadMessage message);
	/// <summary>
	/// 如果点击事件不止一个，可以追加(追加空指针视为清空)
	/// </summary>
	void SetClickAppendCallback(function<void()> append=nullptr);
	/// <summary>
	/// 如果进入事件不止一个，可以追加(追加空指针视为清空)
	/// </summary>
	void SetMouseInAppendCallback(function<void()> append = nullptr);
	/// <summary>
	/// 是否暂停该UI
	/// </summary>
	bool pause=false;
	/// <summary>
	/// 游戏暂停是是否暂停
	/// </summary>
	bool pauseInGamePause = true;
private:
	vector<function<void()>> appendClickCallbacks;
	vector<function<void()>> appendMouseInCallbacks;
};

