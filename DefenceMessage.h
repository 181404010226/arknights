﻿#pragma once
#include <iostream>
#include <functional>
#include "StateMessage.h"
#include "BattleObject.h"
#include "DamageMessage.h"

using namespace std;

class BattleObject;
class DamageMessage;

class DefenceMessage
{
public:
    /// <summary>
    /// 防御值
    /// </summary>
    int physicalDefence=0, magicDefence=0;
    /// <summary>
    /// 通用防御改进函数
    /// </summary>
    function<DefenceMessage* (BattleObject*)> commonModifierCallback;
    function<DefenceMessage* (BattleObject*)> defenceModifierCallback;
    function<void(DamageMessage*, BattleObject*)> defenceCalculationCallback=nullptr;
    DefenceMessage(int physical, int magic);
};

