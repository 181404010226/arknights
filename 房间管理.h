﻿#pragma once
#include "文本矩形.h"
#include"NetworkObject.h"

enum RoomMode {
	房间列表,
	创建房间,
	离开房间
};

class 房间管理:public NetworkObject
{
public:
	void onCreate();
	void GetNetworkMessage(string message);
	// 切换按钮模式
	void ToggleMode(RoomMode mode);
private:
	//显示房间列表
	void createRoomList(string message);
	// 标识是否准备完毕
	void showIsReady();
	// 分析服务端数据
	void analyseString(const std::string& str, std::vector<std::string>& lines);
	// 判断是否在房间内
	bool InRoom=false;
	// 判断是否准备好
	bool myselfIsReady = false;
	bool opponentIsReady = false;
	// 玩家房间所在位置
	文本矩形* MyRoom;
	// 按钮
	文本矩形* Button;
};

