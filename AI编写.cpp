﻿#include "AI编写.h"
#include <iostream>

namespace Utils {

    // 计算两点间的距离
    double distance(Point p1, Point p2) {
        return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
    }

    // 计算两点连线的中点
    Point midpoint(Point p1, Point p2) {
        return { (p1.x + p2.x) / 2, (p1.y + p2.y) / 2 };
    }

    // 计算梯形的四个角点
    vector<Point> getVertices(Point p1, Point p2, Point p3, Point p4) {
        vector<Point> vertices;
        vertices.push_back(p1);
        vertices.push_back(p2);
        vertices.push_back(midpoint(p1, p2));
        vertices.push_back(midpoint(p2, p3));
        vertices.push_back(p3);
        vertices.push_back(p4);
        vertices.push_back(midpoint(p3, p4));
        vertices.push_back(midpoint(p4, p1));
        return vertices;
    }

    // 划分梯形为n*m个方格
    vector<vector<Point>> divideTrapezoid(Point p1, Point p2, Point p3, Point p4, int n, int m) {
        // 存储方格点
        vector<vector<Point>> grids(n, vector<Point>(m));
        // 存储每个方格的四个点
        vector<vector<Point>> rev((n - 1) * (m - 1), vector<Point>(4));
        vector<Point> vertices = getVertices(p1, p2, p3, p4);
        double width = distance(p1,p2) / (m-1);
        double longWidth= distance(p3, p4) / (m - 1);
        // 高度为上底和下底的中点
        double height = distance(vertices[2], vertices[6]) / (n - 1);
        // 计算增量比例
        double dx = (longWidth - width) / (n - 1);
        // 由于实际地图是正方形的投影，所以只能手操了
        double dy[] = { p1.y,115,186,263,344,434 };
        for (int i = 0; i < n; i++) {
            double curWidth = width + dx  * i;
            double curHeight = height ;
            for (int j = 0; j < m; j++) {
                double x = p1.x+(p4.x-p1.x)/(n-1)*i + j * curWidth;
                grids[i][j] = { (float)x, (float)dy[i]};
            }
        }
        // 解析四个顶点
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < m-1; j++) {
                int index = i * (m - 1) + j;
                rev[index][0] = grids[i][j];
                // 避免网格过于靠近影响体验
                rev[index][1] = grids[i + 1][j] - Point{0, 5};
                rev[index][2] = grids[i+1][j+1] - Point{ 5, 5 };
                rev[index][3] = grids[i][j+1] - Point{ 5, 0 };
            }
        }
        return rev;
    }

    // 输出每个方格的四个顶点坐标
    vector<vector<Point>> splitGrid(Point p1, Point p2, Point p3, Point p4, int n, int m) {
        // +1是因为要按照顶点来划分
        return divideTrapezoid(p1, p2, p3, p4, n+1, m+1);
    }

    Point getPolygonCenter(const Point* polygon, int numVertices) {
        float x = 0.0f;
        float y = 0.0f;
        for (int i = 0; i < numVertices; i++) {
            x += polygon[i].x;
            y += polygon[i].y;
        }
        x /= numVertices;
        y /= numVertices;
        return { x,y };
    }
    // 定义一个插值函数
    float lerp(float a, float b, float t) {
        return (1 - t * t) * a + t * t * b;
    }
}