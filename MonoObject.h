﻿#pragma once
#include "Utils.h"
#include <easy2d/easy2d.h>
// using namespace easy2d;

// 前向声明，避免直接using easy2d导致编译缓慢
// 宏定义声明easy2d的gcnew
#define gcnew easy2d::__gc_helper::GCNewHelper::instance << new (std::nothrow)
using Node = easy2d::Node;
using ShapeNode = easy2d::ShapeNode;
using Text = easy2d::Text;
using Input = easy2d::Input;
using Sprite = easy2d::Sprite;
using MouseCode = easy2d::MouseCode;
using Color = easy2d::Color;
using Window = easy2d::Window;

// 物体标签，用于显示顺序控制
enum objectType {
	none,  // 会被设置为99999，因为none还是要显示在最前面
	DonotDestroyed,// 切换界面时不进行销毁的节点
	playerUnit1=10000,  // 不同行的单位显示层级不同
	playerUnit2=20000,
	playerUnit3=30000,
	playerUnit4=40000,
	playerUnit5=50000,
	enemy,
	groud,
	effect,
	camera,
	UI,
	Destroyed,   // 物体将要被销毁时的标签
};

/// <summary>
/// 场景中的物体都要继承
/// </summary>
class MonoObject:virtual public Node
{
public:
	Utils::Rect GetBody();
	// 物体移动速度，单位分别为m/s,没有移动速度的物体不会主动产生碰撞
	float vx = 0;
	float vy = 0;
	// 代表物体类型
	objectType type=none;
	// 物体唯一编号
	int GetHashID();
	// 获取该物体的其他组件
	template <typename T>
	friend T* GetComponent(MonoObject* obj);
	// 能否穿越,为true情况可以撞到其他物体，但不能被其他物体撞到
	bool canThrough = true;
	// 物体创建时调用
	virtual void onCreate();
	// 物体发生碰撞时调用
	virtual void onCrash(MonoObject* collider);
	// 物体摧毁前调用
	virtual void onDestroy();
	// 每帧调用
	virtual void onFrameUpdate();
	// 友元函数，用于修改哈希id
	template <typename T>
	friend T* CreateObject(Utils::Rect body={0,0,0,0}, int flag = 0);
	// 析构函数
	virtual ~MonoObject();
#pragma region 拓展easy2d功能
	void addChild(Node* child);
	void addChild(MonoObject* child);
#pragma endregion

protected:
	MonoObject() = default;
private:
	int hashId = 0;
};

template<typename T>
inline static T* GetComponent(MonoObject* obj)
{
	T* component = dynamic_cast<T*>(obj);
	return component;
}
