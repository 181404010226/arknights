﻿#define _CRT_SECURE_NO_WARNINGS
#include "MonoSystem.h"
#include "NetWorkSystem.h"
#include "WindowSystem.h"
#include "SceneConfig.h"
#include <time.h>
#include "templateSystem.h"
#include "GridSystem.h"
#include "UISystem.h"
#include "BattleSystem.h"
#include "AnimationSystem.h"
using namespace easy2d;

// 注意
// WindowSystem需要在GridSystem后初始化
void InitSystem()
{
    // 注册回调函数
    templateSystem::GetInstance()->PreInit();
    NetworkSystem::GetInstance()->PreInit();
    GridSystem::GetInstance()->PreInit();
    WindowSystem::GetInstance()->PreInit();
    UISystem::GetInstance()->PreInit();
    BattleSystem::GetInstance()->PreInit();
    AnimationSystem::GetInstance()->PreInit();

    // 打印csv表格数据
    templateSystem::GetInstance()->Init();
    // 连接网络
    NetworkSystem::GetInstance()->Init();
    // 初始化网格
    GridSystem::GetInstance()->Init();
    // 初始化窗口
    WindowSystem::GetInstance()->Init();
    UISystem::GetInstance()->Init();
    BattleSystem::GetInstance()->Init();
    AnimationSystem::GetInstance()->Init();
}

int main(void)
{
    if (Game::init())
    {
        Renderer::showFps(true);
        // 修改窗口标题
        Window::setTitle("明日方舟对战版");
        // 创建一个空场景
        Scene* scene = new Scene;
        // 进入 scene 场景
        SceneManager::enter(scene);
        // 初始化根系统
        MonoSystem::GetInstance()->Init();
        // 在场景中加入关卡管理器，而不使用Easy2D原本的管理
        scene->addChild(MonoSystem::GetInstance());
        // 初始化系统
        InitSystem();
        // 初始化窗口
        Game::start(Sceneconfig::GetInstance()->MaxFrame);
        // 释放内存
        delete scene;
    }
    Game::destroy();
	return 0;
}
