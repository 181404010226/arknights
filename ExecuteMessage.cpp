﻿#include "ExecuteMessage.h"
#include "Utils.h"
#include "GridSystem.h"

ExecuteMessage::ExecuteMessage(MessageType type,vector<int> Message)
{
    switch (type) {
    case ACK:
        ACKid = Message[0];
        break;
    case FrameSync:
        FrameCnt = Message[0];
        break;
    case MySummonObject:
        PlayerID = 0;
        ACKid = Message[0];
        FrameCnt = Message[1];
        Posx = Message[2];
        Posy = Message[3];
        Direction = Message[4];
        ObjectType = Message[5];
        break;
    case OppoSummonObject:
        PlayerID = 1;
        ACKid = Message[0];
        FrameCnt = Message[1];
        Posx = Message[2];
        Posy = Message[3];
        Direction = Message[4];
        ObjectType = Message[5];
        // 如果是对手召唤需要进行镜像
        Utils::set_mirror_point(Posx, Posy,
            GridSystem::GetInstance()->BoardHeight,
            GridSystem::GetInstance()->BoardWidth);
        // 异或反向
        Direction ^= 1;
        break;
    case RetreatObject:
        break;
    default:
        break;
    }
}
