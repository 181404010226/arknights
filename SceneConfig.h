﻿#pragma once
#include <fstream>
#include <iostream>


using namespace std;
// 配置全局变量与静态信息
class Sceneconfig
{
public:
	static Sceneconfig* GetInstance();
	const int MaxFrame = 60;
private:
	Sceneconfig();
};

