﻿#pragma once
#include <vector>
#include <set>
#include "BattleObject.h"
using namespace std;

class BattleSystem
{
public:
	static BattleSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	~BattleSystem();
private:
	BattleSystem() = default;
	// 存储所有物体
	set<BattleObject*> BattleObjects;
};


