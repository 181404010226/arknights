﻿#pragma once
#include "GridObject.h"
#include "BattleObject.h"
#include "AnimationObject.h"

class 基地 :public GridObject, public BattleObject, public AnimationObject
{
public:
	void onCreate();
	void onFrameUpdate(); 
	void onDestroy();
};

