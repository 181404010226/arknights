﻿#pragma once
#include "MonoObject.h"
#include "网格单位贴图.h"
#include "enum.h"
#include <tuple>
using namespace std;

class GridObject :
    virtual public MonoObject
{
public:
    /// <summary>
    /// 高台/地面/特种
    /// </summary>
    GridType gridType;
    /// <summary>
    /// 干员名，用于对手同步召唤相同的干员
    /// </summary>
    OperatorName typeName;
    // 当前单位所在网格
    tuple<int, int> GridPos = { -1, -1 };
    /// <summary>
    /// 设置朝向(上下左右)
    /// </summary>
    void SetDirection(int number);
    /// <summary>
    /// 获取朝向(上下左右)
    /// </summary>
    int GetDirection();
    // 析构函数
    virtual ~GridObject();
    /// <summary>
    /// 召唤帧
    /// </summary>
    int SummonFrame=-1;
    /// <summary>
    /// 所属玩家Id，0为我方，1为敌方
    /// </summary>
    int PlayerId;
protected:
    int m_direction;
};

