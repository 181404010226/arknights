﻿#pragma once
#include "AnimationObject.h"
#include <set>
using namespace std;

class AnimationSystem
{
public:
	static AnimationSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	~AnimationSystem();
private:
	set<AnimationObject*> AnimationObjects;
};

