﻿#pragma once
#include <vector>
#include <easy2d/easy2d.h>
using namespace std;

using Point = easy2d::Point;


namespace Utils {
    /// <summary>
    /// 左上角，右上角,右下角，左下角
    /// </summary>
    vector <vector<Point>> splitGrid(Point p1, Point p2, Point p3, Point p4, int n, int m);
    // 获取一个多边形的中心点
    Point getPolygonCenter(const Point* polygon, int numVertices);
    // 定义一个插值函数
    float lerp(float a, float b, float t); 
}

