﻿#include "BattleSystem.h"
#include <fstream>
#include "MonoSystem.h"
#include "GridObject.h"


BattleSystem* BattleSystem::GetInstance()
{
    // 饿汉式
    static BattleSystem m_Instance;
    return &m_Instance;
}

void BattleSystem::Run()
{
    for (auto it = BattleObjects.begin(); it != BattleObjects.end(); ++it)
    {
        GridObject* gridObj = GetComponent<GridObject>(*it);
        // 召唤成功之后才执行战斗更新
        if (gridObj->SummonFrame>=0) (*it)->onBattleUpdate();
    }
    for (auto it = BattleObjects.begin(); it != BattleObjects.end(); ++it)
    {
        // 保证负数血量同一帧死亡
        if ((*it)->Life <= 0) DestroyObject(*it);
    }
    
}

bool BattleSystem::Init()
{
    
    return true;
}

void BattleSystem::PreInit()
{
    // 注册回调函数
    MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
        BattleObject* obj = dynamic_cast<BattleObject*>(newObj);
        if (obj) BattleObjects.insert(obj);
        });
    MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
        BattleObject* obj = dynamic_cast<BattleObject*>(newObj);
        if (obj) BattleObjects.erase(obj);
        });
}

BattleSystem::~BattleSystem()
{
}