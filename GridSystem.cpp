﻿#include "GridSystem.h"
#include <fstream>
#include "MonoSystem.h"
#include "NetWorkSystem.h"
#include "AI编写.h"
#include "Debug.h"
#include "幽灵鲨.h"
#include "甜甜圈.h"
#include "基地.h"
#include "UISystem.h"


GridObject* GridSystem::GetSummonObject()
{
    return summonObject;
}

void GridSystem::SetSummonObject(GridObject* obj)
{
    summonObject = obj;
}

GridSystem* GridSystem::GetInstance()
{
    // 饿汉式
    static GridSystem m_Instance;
    return &m_Instance;
}

void GridSystem::Run()
{
    // 根据有无正在召唤的单位来刷新阶段
    if (GetSummonObject() == nullptr) isDirectPhase = false;
}


void GridSystem::setToGrid(GridObject* gridObject, int x, int y)
{
    // 预览召唤单位
    Rect rect = shapeGrids[x][y]->ButtonArea->getBoundingBox();
    Point center = Point(rect.getLeft() + rect.getWidth() / 2, rect.getBottom() - rect.getHeight() / 2);
    // 放置到多边形中心
    gridObject->setPos(center- MonoSystem::GetInstance()->RootNode->getPos());
    gridObject->GridPos = make_tuple(x, y);
    // 更改显示层级
    gridObject->type = objectType((x+1) * gridObject->type);
}

Function<void()>  GridSystem::responseMouseClick(int gridIndexi,int gridIndexj) {
    return [this, gridIndexi, gridIndexj]() {
        // 必须要保证点击的位置和干员当前预览位置相同
        if (GetSummonObject() != nullptr && gridIndexi== get<0>(GetSummonObject()->GridPos)
            && gridIndexj == get<1>(GetSummonObject()->GridPos))
        {
            // 关闭网格监听避免bug
            closeGridsListener();
            isDirectPhase = true;
            // 对准备召唤的单位的朝向进行控制
            control = CreateObject<朝向控制>();
            control->removeFromParent();
            control->controlObject(GetSummonObject());
            root->addChild(control);
        }
        cout << "当前点击的网格为" << gridIndexi << " " << gridIndexj << endl;
    };
}

void GridSystem::registerGirdEvent(ShapeNode* shapeNode,int gridIndexi, int gridIndexj)
{
    // 响应事件的控件
    文本矩形* button=CreateObject<文本矩形>();
    button->removeFromParent();
    root->addChild(button);
#pragma region 这句话不加shapeNode会被gc清除
    button->addChild(shapeNode);
#pragma endregion
    // 避免被清除
    button->type = DonotDestroyed;
    // 重定义响应区域
    button->ButtonArea = shapeNode;
    button->SetMouseInAppendCallback([this, gridIndexi, gridIndexj]() {
        // 检测合法性
        if (GetSummonObject() != nullptr && objectInGrids[gridIndexi][gridIndexj] == NULL && !isDirectPhase)
        {
            // 判断地形是否匹配
            if (GridsType[gridIndexi][gridIndexj]!=Forbid && 
                (GridsType[gridIndexi][gridIndexj]==GetSummonObject()->gridType ||
                    GetSummonObject()->gridType==Special))
            {
                setToGrid(GetSummonObject(), gridIndexi, gridIndexj);
                // 更新贴图大小
                GetSummonObject()->SetDirection(3);
                return;
            }
        }
        // 设置鼠标效果
        Window::setCursor(Window::Cursor::No);
    });
    button->OnClick = responseMouseClick(gridIndexi,gridIndexj);
    shapeGrids[gridIndexi][gridIndexj] = button;
}

vector<tuple<int, int>> GridSystem::GetRangeInGrids(BattleObject* battleObj)
{
    vector<tuple<int, int>> rev;
    // 获取网格部分
    GridObject* gridObj = GetComponent<GridObject>(battleObj);
    if (gridObj == nullptr) return rev;
    vector<int> range;
    tuple<int, int> originPos;// 初始位置
    int direction;
    // 用于计算偏移
    tuple<int, int> offset[4] = { make_tuple(-1,0),make_tuple(1,0),make_tuple(0,-1),make_tuple(0,1) };
    switch (battleObj->attack->attackType)
    {
    case NormalAttack:
        // 初始位置
        originPos = gridObj->GridPos;
        direction = gridObj->GetDirection();
        range = battleObj->attack->AttackRange;
        // 遍历攻击范围数组
        for (int i = 0; i < range.size(); i += 2)
        {
            // 偶数位代表攻击长度,奇数代表攻击起始位置
            for (int attackLen = range[i]; attackLen < range[i] + range[i + 1]; attackLen++)
            {
                // 获取面向方向的偏移
                int offsetx = get<0>(offset[direction]) * attackLen;
                int offsety = get<1>(offset[direction]) * attackLen;
                // 获取侧方向的偏移
                int sideOffsetx = get<1>(offset[direction]) * (i / 2);
                int sideOffsety = get<0>(offset[direction]) * (i / 2);
                int a = get<0>(originPos) + sideOffsetx + offsetx;
                int b = get<1>(originPos) + sideOffsety + offsety;
                if (a >= 0 && a < BoardHeight && b >= 0 && b < BoardWidth)
                    rev.push_back(make_tuple(a,b));
                if (i == 0) continue;
                // 镜像
                a = get<0>(originPos) - sideOffsetx + offsetx;
                b = get<1>(originPos) - sideOffsety + offsety;
                if (a >= 0 && a < BoardHeight && b >= 0 && b < BoardWidth)
                    rev.push_back(make_tuple(a, b));
            }
        }
        break;
    default:
        break;
    }
    return rev;
}


bool GridSystem::Init()
{

#pragma region 网格初始化
    // 获取一个根节点来挂载到屏幕上
    root = MonoSystem::GetInstance()->RootNode;
    // 初始化网格大小,并赋值nullptr
    objectInGrids.resize(BoardHeight, vector<GridObject*>(BoardWidth));
    shapeGrids.resize(BoardHeight, vector<文本矩形*>(BoardWidth));
    // 记录每个格子代表的多边形
    vector<vector<Point>> grids = Utils::splitGrid( { 126,56 }, {681, 56}, { 751,436 }, { 55,436 } , BoardHeight, BoardWidth);
    // Assuming you have access to a graphics context, such as a window or canvas
    for (int i = 0; i < BoardHeight; i++)
    {
        for (int j = 0; j < BoardWidth; j++)
        {
            objectInGrids[i][j] = nullptr;
            int index = i * BoardWidth + j;
            // Create a new polygon object
            vector<Point> points = grids[index];
            if (points.size() == 0) continue;
            const Point* polygon = &points[0]; // 向量的底层是连续的一维数组

            // Create a new shape node with the polygon
            auto shape = ShapeNode::createPolygon(polygon, 4);
            // 存储到网格
            shape->setStrokeColor((Color::Blue));
            // Set the fill color to orange
            shape->setFillColor(Color::Orange);
            // Set the opacity to 0 to make the shape invisible
            shape->setOpacity(0);
            // 注册网格事件
            registerGirdEvent(shape,i, j);
        }
    }
#pragma endregion
    return true;
}

void GridSystem::PreInit()
{
    // 注册回调函数
    MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
        GridObject* obj = dynamic_cast<GridObject*>(newObj);
        if (obj) AllGridObjects.insert(obj);
        });
    MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
        GridObject* obj = dynamic_cast<GridObject*>(newObj);
        if (obj)
        {
            AllGridObjects.erase(obj);
            // 如果是被摧毁的单位则清空对应网格的指针
            auto battleobj = GetComponent<BattleObject>(obj);
            if (battleobj && battleobj->Life <= 0)
                objectInGrids[get<0>(obj->GridPos)][get<1>(obj->GridPos)] = nullptr;
            // 如果是要召唤的单位则删除朝向控制
            if (isDirectPhase &&
                control->GetControlObject()->GetHashID()== obj->GetHashID())
                control->controlObject(nullptr);
            }
        });
}

void GridSystem::reset()
{
    openGridsListener();
    isDirectPhase = false;
    summonObject = nullptr;
    for (int i = 0; i < BoardHeight; i++)
        for (int j = 0; j < BoardWidth; j++)
            objectInGrids[i][j] = nullptr;
}

void FliterObjectInVector(vector<BattleObject*> &vec, Function<bool(BattleObject*)> judge)
{
    for (auto it = vec.begin(); it != vec.end(); ) {
        if (judge(*it)){
            it = vec.erase(it);
        }
        else {
            ++it;
        }
    }
}

vector<BattleObject*> GridSystem::GetAttackRangeObjects(BattleObject* battleObj)
{
    vector<BattleObject*> rev;
    // 获取攻击范围内的所有可战斗单位
    auto targetGrids = GetRangeInGrids(battleObj);
    // 记录最低血量的友军
    BattleObject* LowLifeFriend=nullptr;
    // 记录最早召唤的敌人
    BattleObject* FastEnemy = nullptr;
    for (auto targetGrid : targetGrids)
    {
        GridObject* gridObj = objectInGrids[get<0>(targetGrid)][get<1>(targetGrid)];
        if (gridObj)
        {
            BattleObject* battleObj = GetComponent<BattleObject>(gridObj);
            if (battleObj)
            {
                rev.push_back(battleObj);
            }
        }
    }
    bool needSamePlatform = false;
    if (battleObj->attack->rangeRequest == SamePlatform) needSamePlatform = true;
    // 先排除通用情况
    FliterObjectInVector(rev, [battleObj, needSamePlatform](BattleObject* fliterObj) {
        if (fliterObj->GetHashID() == battleObj->GetHashID()) return true;
        if (fliterObj->state->ExistState(unSelectable)) return true;
        return false;
        });
    switch (battleObj->attack->attackType)
    {
    case Enemy:
        break;
    case AllEnemy:
        // 排除所有友军
        FliterObjectInVector(rev, [battleObj, needSamePlatform](BattleObject* fliterObj) {
            GridObject* fliter = GetComponent<GridObject>(fliterObj);
            GridObject* attacker = GetComponent<GridObject>(battleObj);
            // 过滤掉不在一个平台的
            if (needSamePlatform && fliter->gridType != attacker->gridType) return true;
            if (fliter->PlayerId == attacker->PlayerId) return true;
            return false;
            });
        return rev;
    case Friend:
        break;
    case AllFriend:
        // 排除所有敌人
        FliterObjectInVector(rev, [battleObj, needSamePlatform](BattleObject* fliterObj) {
            GridObject* fliter = GetComponent<GridObject>(fliterObj);
            GridObject* attacker = GetComponent<GridObject>(battleObj);
            // 过滤掉不在一个平台的
            if (needSamePlatform && fliter->gridType != attacker->gridType) return true;
            if (fliter->PlayerId != attacker->PlayerId) return true;
            return false;
            });
        break;
    case All:
        break;
    default:
        break;
    }
    return rev;
}

void GridSystem::ShowObjectAttackRange(BattleObject* battleObj)
{
    DisShowObjectAttackRange();
    // 获取攻击范围内的所有网格，然后设置透明度为50%模拟显示攻击范围.
    auto targetGrids=GetRangeInGrids(battleObj);
    for (auto targetGrid : targetGrids)
    {
        shapeGrids[get<0>(targetGrid)][get<1>(targetGrid)]
            ->ButtonArea->setOpacity(0.5);
    }
}

void GridSystem::DisShowObjectAttackRange()
{
    for (int i = 0; i < BoardHeight; i++)
        for (int j = 0; j < BoardWidth; j++)
            shapeGrids[i][j]->ButtonArea->setOpacity(0);
}

GridObject* GridSystem::GetGridObejct(int i, int j)
{
    return objectInGrids[i][j];
}

GridSystem::~GridSystem()
{
}

int GridSystem::judgeSummonState(GridObject* obj, SummonMessage msg)
{
    if (objectInGrids[msg.x][msg.y] == nullptr) return 1;
    else
    {
        // 对比召唤时间
        int gridFrame = objectInGrids[msg.x][msg.y]->SummonFrame, objFrame = msg.SummonFrame;
        // 同一帧召唤的话直接两边都失败
        if (gridFrame == objFrame) return -1;
        if (gridFrame > objFrame) return 1;
        else  return 0;
    }
    return 0;
}

GridObject* GridSystem::Summon(SummonMessage msg)
{
    GridObject* obj=nullptr;
    switch (msg.operatorName)
    {
    case OperatorName::甜甜圈:
        obj = CreateObject<甜甜圈>();
        // +10000是因为召唤时间早的可以替换掉召唤时间晚的
        msg.SummonFrame += 10000;
        break;
    case OperatorName::幽灵鲨:
        obj = CreateObject<幽灵鲨>();
        break;
    case OperatorName::基地:
        obj = CreateObject<基地>();
        break;
    default:
        throw "未找到服务端消息对应的干员";
        break;
    }
    int summonFlag = judgeSummonState(obj, msg);
    if (summonFlag == 1) // 召唤成功
    {
        DestroyObject(objectInGrids[msg.x][msg.y]);
        objectInGrids[msg.x][msg.y] = nullptr;
        // 确认占用召唤位置
        objectInGrids[msg.x][msg.y] = obj;
        // 设置召唤属性
        obj->PlayerId = msg.PlayerID;
        obj->SummonFrame = msg.SummonFrame;
        setToGrid(obj, msg.x, msg.y);
        // 设置面向
        obj->SetDirection(msg.direction);
    }
    else if (summonFlag == 0) // 召唤失败
    {
        DestroyObject(obj);
    }
    else if (summonFlag == -1)// 两边同时召唤，只能设计成都不召唤
    {
        DestroyObject(objectInGrids[msg.x][msg.y]);
        DestroyObject(obj);
        objectInGrids[msg.x][msg.y] = nullptr;
        return nullptr;
    }
    if (objectInGrids[msg.x][msg.y] != nullptr)
    {
        // 开启动画
        GetComponent<AnimationObject>(objectInGrids[msg.x][msg.y])->ResetToState(UnitAnimationState::Idle);
        // 更改显示层级
         objectInGrids[msg.x][msg.y]->setOrder(objectInGrids[msg.x][msg.y]->type);
         // 如果召唤失败，恢复冷却时间，刷新UI界面
         if (summonFlag<=0 && msg.PlayerID==0)
             UISystem::GetInstance()->BroadMessage({ summonResultMessage,{msg.x,msg.y},false });
    }
    return objectInGrids[msg.x][msg.y];
}



void GridSystem::closeGridsListener()
{
    for (int i = 0; i < BoardHeight; i++)
        for (int j = 0; j < BoardWidth; j++)
            shapeGrids[i][j]->pause=true;
}

void GridSystem::openGridsListener()
{
    for (int i = 0; i < BoardHeight; i++)
        for (int j = 0; j < BoardWidth; j++)
            shapeGrids[i][j]->pause = false;
}
