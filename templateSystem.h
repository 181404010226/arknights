﻿#pragma once
#include <vector>
#include "templateObject.h"
#include <set>
using namespace std;

class templateSystem
{
public:
	static templateSystem* GetInstance();
	void Run();
	bool Init();
	void PreInit();
	~templateSystem();
private:
	templateSystem() = default;
	// 存储所有物体
	set<templateObject*> templateObjects;
};

