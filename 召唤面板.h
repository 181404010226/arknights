﻿#pragma once
#include "UIObject.h"
#include "文本矩形.h"
#include "侧边栏.h"
#include <unordered_map>
#include <map>

class 召唤面板 :public UIObject
{
public:
	const float CostPerSecond = 5.0f;
	void onCreate();
	void onFrameUpdate();
	// 接收广播消息
	void OnGetUIMessage(UIBroadMessage message);
private:
	侧边栏* sideBar;
	// 当前点击的UI
	文本矩形* clickedUI=nullptr;
	// 记录点击的UI所代表的图片的路径
	string selectedObjectPath;
	// 代替CP条
	文本矩形* CPBar = nullptr;
	// 每调用一次会下底部一次创建一个头像，并设置好部署时间和部署费用
	template <typename T>
	void createSummonPanel(string picturePath, float deploymentTime, int cost);
	void createCPBar();
	// 蒙版，用来处理倒计时
	unordered_map<文本矩形*, float> masks;
	// 用于处理召唤结果,此处不能使用unordered_map,因为不支持
	map<tuple<int,int>, 文本矩形*> maskCallbak;
};

