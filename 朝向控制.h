﻿#pragma once
#include "GridObject.h"
#include "文本矩形.h"
using namespace std;
class 朝向控制:public MonoObject
{
public:
	void onCreate();
	void onFrameUpdate();
	void controlObject(GridObject* target);
	GridObject* GetControlObject();
private:
	// 封装完成选择的函数
	void selected(string text);
	GridObject* targetObject;
	vector<文本矩形*> Directions;
	void addDirectionButton( Point offset, string text, int direction);
	// 方向键的回调函数
	Function<void()> createClickCallback(string text);
};

