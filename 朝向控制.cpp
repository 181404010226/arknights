﻿#include "朝向控制.h"
#include "GridSystem.h"
#include "NetWorkSystem.h"
#include "MonoSystem.h"
#include "WindowSystem.h"
#include "BattleObject.h"
#include "UISystem.h"
#include <tuple>

void 朝向控制::onCreate()
{
    // 开启侧边栏
    UISystem::GetInstance()->BroadMessage({ gridClickedMessage,make_tuple(0,0),true});
}

void 朝向控制::onFrameUpdate()
{
    // 解决外部清除targetObject导致的问题
    if (targetObject == nullptr) return;
    // 如果选定的格子对手先召唤了干员，则关闭朝向控制
    if (GridSystem::GetInstance()->
        GetGridObejct(get<0>(targetObject->GridPos), get<1>(targetObject->GridPos)))
    {
        selected("关");
    }
}



// 用于创建各个按钮的回调函数
Function<void()> 朝向控制::createClickCallback(string text) {
    return [this, text]() {
        selected(text);
    };
}

void 朝向控制::addDirectionButton(Point offset, string text, int direction) {
    // 解决外部清除targetObject导致的问题
    if (targetObject == nullptr) return;
    auto square = CreateObject<文本矩形>({ 0,0,50, 50 });
    square->setPos(targetObject->getPos() + offset);
    square->text->setText(text);
    addChild(square);
    // 绑定点击和鼠标悬停的事件
    square->OnClick=createClickCallback(text);
    square->SetMouseInAppendCallback([this, direction]() {
        if (targetObject == nullptr) return;
        GridSystem::GetInstance()->ShowObjectAttackRange(GetComponent<BattleObject>(targetObject));
        targetObject->SetDirection(direction);
    });
    // 内存管理
    Directions.push_back(square);
}

void 朝向控制::controlObject(GridObject* target)
{
    // 解决外部清除targetObject导致的问题
    if (target == nullptr)
    {
        selected("关");
        return;
    }
    targetObject = target;
    addDirectionButton(Point(0, -100), "上", 0);
    addDirectionButton(Point(0, 100), "下", 1);
    addDirectionButton(Point(-100, 0), "左", 2);
    addDirectionButton(Point(100, 0), "右", 3);
    addDirectionButton(Point(-100, -100), "关", 3);
    // 移动画面使得干员位于中心
    WindowSystem::GetInstance()->SetTargetPos({ 75,0  });
}

GridObject* 朝向控制::GetControlObject()
{
    return targetObject;
}

void 朝向控制::selected(string text)
{
    // 解决外部清除targetObject导致的问题
    if (targetObject == nullptr) return;
    // 清除召唤
    GridSystem::GetInstance()->openGridsListener();
    DestroyObject(this);
    for (auto it = Directions.begin(); it != Directions.end(); ++it)
        DestroyObject(*it);
    GridSystem::GetInstance()->isDirectPhase = false;
    GridSystem::GetInstance()->DisShowObjectAttackRange();
    // 还原画面
    WindowSystem::GetInstance()->SetTargetPos({ 0, 0 });
    // 关闭侧边栏
    UISystem::GetInstance()->BroadMessage({ gridClickedMessage,make_tuple(0,0),false });
    if (text != "关")
    {
        auto pos = GridSystem::GetInstance()->GetSummonObject()->GridPos;
        // 广播UI消息,表面操作成功（这一步需要在通知召唤之前，否则消息发送顺序不正确）
        UISystem::GetInstance()->BroadMessage({ directionChooseMessage,pos });
        // 发送召唤信息,使得双方同步召唤
        int direction = GridSystem::GetInstance()->GetSummonObject()->GetDirection();
        NetworkSystem::GetInstance()->SummonObject(
            get<0>(pos), get<1>(pos), direction, targetObject->typeName);
        // 召唤延迟预览
        DestroyObject(GridSystem::GetInstance()->GetSummonObject());
        GridSystem::GetInstance()->Summon({ get<0>(pos), get<1>(pos), 3,0,
            WindowSystem::GetInstance()->GetBattleFrameCnt(), OperatorName::甜甜圈 });
        GridSystem::GetInstance()->SetSummonObject(nullptr);
    }
}
