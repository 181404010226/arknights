﻿#pragma once
#include <iostream>
#include <queue>
#include "NetworkObject.h"
#include <set>
#include "enum.h"
using namespace std;


/// <summary>
/// 用于获取网络信息
/// </summary>

class NetworkSystem
{
public:
	static NetworkSystem* GetInstance();
	/// <summary>
	/// 取出一条收到的网络消息
	/// </summary>
	string PopNetworkMessage();
	/// <summary>
	/// 添加一条网络消息,是否广播到服务器,是否广播给自己
	/// </summary>
	void AppendNetworkMessage(string message, bool board,bool self=true);
	/// <summary>
	/// 封装召唤干员的消息
	/// </summary>
	void SummonObject(int x, int y, int direction,OperatorName type);
	/// <summary>
	/// 网络系统帧更新
	/// </summary>
	void Run();
	bool Init();
	void PreInit();
	~NetworkSystem();
private:
	NetworkSystem() = default;
	// 存储所有网络物体
	set<NetworkObject*> NetworkObjects;
	//保存消息队列
	queue<string> networkMessage;
};