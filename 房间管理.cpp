﻿#include "房间管理.h"
#include "NetWorkSystem.h"
#include "MonoObject.h"
#include "MonoSystem.h"
#include "PlayerSystem.h"
#include <functional>
#include <sstream> 
#include "WindowSystem.h"
using Size = easy2d::Size;
using namespace std;

inline void joinRoom(string message)
{
	int x = message.find("Room");
	int y = message.find(":");
	string RoomId = message.substr(x + 4, y - x - 4);
	NetworkSystem::GetInstance()->AppendNetworkMessage("joinRoom " + RoomId,true);
	NetworkSystem::GetInstance()->AppendNetworkMessage("showRooms ", true);
}

void beginGame()
{
	WindowSystem::GetInstance()->PlayWindow();
}

void 房间管理::analyseString(const std::string& str, std::vector<std::string>& lines) {
	std::istringstream iss(str);
	std::string line;
	while (std::getline(iss, line)) {
		if (line.find(":") != std::string::npos) {
			lines.push_back(line);
			// 代表房间中的人数大于1
			if (line.find(",") != std::string::npos)
			{
				// 添加准备按钮
				文本矩形* readyButton = CreateObject<文本矩形>({0,0,50,50});
				// 位于按钮右边
				readyButton->setPos(200,100);
				readyButton->button->setFillColor(Color::Green);
				readyButton->text->setText("准备");
				Button->addChild(readyButton);
				// 绑定点击效果
				readyButton->OnClick=[this]() {
					myselfIsReady = true;
					showIsReady();
					NetworkSystem::GetInstance()->AppendNetworkMessage("ready", true, false);
					if (opponentIsReady) beginGame();
				};
			}
			else {
				myselfIsReady = false;
				opponentIsReady = false;
			}
		}
	}
}

void createBezel(文本矩形* parent) {
	文本矩形* bezel = gcnew 文本矩形(Size(Window::getSize().width, Window::getSize().height / 2));
	bezel->setName("list");
	bezel->setPos(parent->getPosX(), -Window::getSize().height / 4);
	parent->addChild(bezel);
}

void 房间管理::createRoomList(string message) {
	// 分割字符串
	std::vector<std::string> lines;
	analyseString(message, lines);
	// 移除之前的列表栏
	Button->removeChildren("list");
	// 创建列表栏
	createBezel(Button);

	int i = 0;
	for (const auto& line : lines) {
		文本矩形* rect = CreateObject<文本矩形>({ 0,0,Window::getSize().width, 50 });
		rect->setPos(rect->getPos() + Point(0, i * 50));
		rect->button->setFillColor(Color::Black);
		// 绘制边框
		rect->button->setStrokeWidth(5);
		rect->button->setStrokeColor(Color::Gold);
		rect->text->setText(line);
		// 从父节点移除
		rect->removeFromParent();
		Button->getChild("list")->addChild(rect);
		// 判断是否是玩家所在房间
		if (line.find(PlayerSystem::GetInstance()->getPlayerName()) != string::npos)
		{
			MyRoom = rect;
		}
		// 绑定加入房间的回调函数
		rect->OnClick= [line]() {
				joinRoom(line);
		};
		i++;
	}
	// 通过查找列表名来判断是否在房间内
	if (message.find(PlayerSystem::GetInstance()->getPlayerName())
		!= string::npos)
	{
		InRoom = true;
	}
	else InRoom = false;
	// 根据是否在房间内决定按钮的变化
	if (InRoom) ToggleMode(离开房间);
	else ToggleMode(创建房间);
}

void 房间管理::showIsReady()
{
	if (myselfIsReady)
	{
		auto greenCircle=ShapeNode::createCircle(20);
		greenCircle->setFillColor(Color::Green);
		greenCircle->setPos(50, 10);
		MyRoom->button->addChild(greenCircle);
	}
	if (opponentIsReady)
	{
		auto greenCircle = ShapeNode::createCircle(20);
		greenCircle->setFillColor(Color::Green);
		greenCircle->setPos(Window::getWidth() - 50, 10);
		MyRoom->button->addChild(greenCircle);
	}
}

void 房间管理::onCreate()
{
	// 修改轴心
	this->setAnchor(0.5, 0.5);
	// 创建矩形
	Button = CreateObject<文本矩形>({ 0,0,getSize().width,getSize().height });
	this->addChild(Button);
	ToggleMode(房间列表);
}


void 房间管理::GetNetworkMessage(string message)
{
	// 判断对手是否准备好
	if (message.find(":") != string::npos && message.find("ready") != string::npos)
	{
		opponentIsReady = true;
		showIsReady();
		if (myselfIsReady) beginGame();
	}
	else if (message.find(":")!=string::npos || message.find("showRooms") != string::npos)
		createRoomList(message);
	
}

// 用于创建控制器的回调函数
function<void()> createCallback(string message) {
	return [message]() {
		std::cout << message << std::endl;
		NetworkSystem::GetInstance()->AppendNetworkMessage(message, true);
	};
}

void 房间管理::ToggleMode(RoomMode mode) {
	Button->button->removeAllListeners();
	// 玩家房间
	string roomName;
	switch (mode) {
	case 房间列表:
		Button->text->setText("房间列表");
		Button->OnClick=createCallback("showRooms");
		break;
	case 创建房间:
		Button->text->setText("创建房间");
		roomName = std::to_string(rand() % 256);
		Button->OnClick = createCallback("createRoom " + roomName);
		break;
	case 离开房间:
	{
		// 按钮事件只能绑定一个
		Button->text->setText("离开房间");
		auto callback = []() {
				std::cout << "离开房间" << std::endl;
				NetworkSystem::GetInstance()->AppendNetworkMessage("leaveRoom", true);
				NetworkSystem::GetInstance()->AppendNetworkMessage("showRooms ", true);
		};
		Button->OnClick = callback;
		break;
	}
	default:
		break;
	}
}
