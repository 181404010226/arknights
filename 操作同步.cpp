﻿#include "操作同步.h"
#include "Utils.h"
#include "MonoSystem.h"
#include "PlayerSystem.h"
#include "WindowSystem.h"
#include "文本矩形.h"
#include "GridSystem.h"
#include "幽灵鲨.h"

MessageType parseMessageType(string message) {
    int pos = 0;
    // 有没有“：”决定了是自己的操作消息还是对手的操作消息
    if (message.find(":")!=string::npos) pos= message.find(":") +1;
    // Extract the message type from the message string
    // Then, convert the message type string to an enum value
    if (message.substr(pos, 4) == "SYNC") {
        return FrameSync;
    }
    else if (message.substr(pos, 3) == "ACK") {
        return ACK;
    }
    else if (message.substr(pos, 6) == "SUMMON") {
        if (pos == 0) return MySummonObject;
        else return OppoSummonObject;
    }
    else if (message.substr(pos, 6) == "RETREAT") {
        return RetreatObject;
    }
    else {
        return Unknown;
    }
}


void 操作同步::onCreate()
{
    
}

void 操作同步::onFrameUpdate()
{
    // 判断当前帧是否有需要执行的操作（当前只处理了召唤操作）
    if (operators.size() > 0 && operators.begin()->first == 
        WindowSystem::GetInstance()->GetBattleFrameCnt())
    {
        ExecuteMessage* msg = operators.begin()->second;
        GridSystem::GetInstance()->Summon(
            { msg->Posx, msg->Posy, msg->Direction,msg->PlayerID,
            WindowSystem::GetInstance()->GetBattleFrameCnt(),
            OperatorName(msg->ObjectType) });
        operators.erase(operators.begin());
        delete msg;
    }
    // 判断有没有需要对方回应的操作
    if (ACKmessage.size() > 0)
    {
        // 每帧给对手发一遍
        NetworkSystem::GetInstance()->AppendNetworkMessage(ACKmessage.front().second, true, false);
    }
    // NetworkSystem::GetInstance()->AppendNetworkMessage("SUMMON " +to_string(ACKid++), true, false);
    FrameSynchron();
}

void 操作同步::FrameSynchron()
{
    // 当前系统暂停情况下，等待对手恢复到于我同帧
    if (MonoSystem::GetInstance()->Pause==true)
    {
        if (OpponentFrame >= WindowSystem::GetInstance()->GetBattleFrameCnt())
            MonoSystem::GetInstance()->Pause = false;
    }
    else if (OpponentFrame + FrameLimit < WindowSystem::GetInstance()->GetBattleFrameCnt())
    {
        // 当对手落后我方指定帧时暂停
        cout << "暂停ing" << endl;
        MonoSystem::GetInstance()->Pause = true;
    }
}

void 操作同步::GetNetworkMessage(string message)
{
    vector<string> part = Utils::splitProtocol(message, " ");
    // Parse the incoming message and extract the message type
    MessageType type = parseMessageType(part[0]);
    // 除part[0]以外剩下的都是数字消息
    vector<int> positions;
    for (int i = 1; i < part.size(); i++) positions.push_back(Utils::numberInStr(part[i]));
    ExecuteMessage* msg = new ExecuteMessage(type, positions);

    // Handle the message based on its type
    switch (type) {
    case FrameSync:
        OpponentFrame = msg->FrameCnt;
        FrameSynchron();
        break;
    case ACK:
        if (ACKmessage.size() > 0)
        {
            if (ACKmessage.front().first==msg->ACKid) ACKmessage.pop();
        }
        break;
    case MySummonObject:
        // 记录操作，必要时持续发送
        ACKmessage.push(make_pair(PlayerSystem::GetInstance()->ACKid, message));
        PlayerSystem::GetInstance()->ACKid++;
        operators.insert(make_pair(msg->FrameCnt, msg));
        break;
    case OppoSummonObject:
        // 避免后手重复召唤多次
        if (oppoACK.find(msg->ACKid) == oppoACK.end())
        {
            operators.insert(make_pair(msg->FrameCnt, msg));
            oppoACK.insert(make_pair(msg->ACKid,true));
            // 召唤一个示意图
            GridSystem::GetInstance()->Summon(
                { msg->Posx, msg->Posy, 0,msg->PlayerID,
                WindowSystem::GetInstance()->GetBattleFrameCnt(),
                OperatorName::甜甜圈 });
        }
        // 回应成功
        NetworkSystem::GetInstance()->AppendNetworkMessage("ACK " + to_string(msg->ACKid), true, false);
        break;
    case RetreatObject:
        // Extract the object ID from the message and remove the corresponding object from the game world
        break;
    default:
        // Handle unknown message types
        break;
    }
}
