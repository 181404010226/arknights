﻿#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include "NetworkSystem.h"
#include "PlayerSystem.h"
#include "WindowSystem.h"
#include <WS2tcpip.h>
#include <WinSock2.h>
#include <Windows.h>
#include <stdio.h>
#include <cstdio>
#include <thread>
#include <mutex>
#include "MonoObject.h"
#include "MonoSystem.h"
#include "NetworkObject.h"
#pragma comment(lib, "ws2_32.lib")


// 全局变量创建socket
SOCKET hSock;
// 全局变量存储待发送到服务器的消息
queue<string> BoardingMessage;
std::mutex sendMtx; // 保护counter
std::mutex readMtx; // 保护counter


// 多线程接收消息
inline unsigned RecvMsg()
{
	char msg[1024];
	while (1)
	{
		int len = recv(hSock, msg, sizeof(msg) - 1, 0);
		if (len == -1)
		{
			cout << WSAGetLastError() << endl;
			return -1;
		}
		msg[len] = '\0';
		printf("%s\n", msg);
		NetworkSystem::GetInstance()->AppendNetworkMessage(msg, false);
	}
	return 0;
}

// 多线程发送消息
inline unsigned SendMsg()
{
	// 多线程内只能操作局部变量
	char msg[1024];
	while (1)
	{
		// 加锁
		sendMtx.lock();
		if (BoardingMessage.size() > 0)
		{
			string temp = BoardingMessage.front();
			strcpy_s(msg, temp.c_str());
			BoardingMessage.pop();
			// Add delimiter to the end of the message
			strcat_s(msg, "|");
			send(hSock, msg, strlen(msg), 0);
		}
		// 解锁
		sendMtx.unlock();
	}
	return 0;
}

NetworkSystem* NetworkSystem::GetInstance()
{
	// 饿汉式
	static NetworkSystem m_Instance;
	return &m_Instance;
}


string NetworkSystem::PopNetworkMessage()
{
	if (networkMessage.size() == 0) return "";
	string msg = networkMessage.front();
	networkMessage.pop();
	return msg;
}

void NetworkSystem::AppendNetworkMessage(string message, bool board, bool self)
{
	readMtx.lock();
	// 获取时间并添加到字符串中
	/* SYSTEMTIME st = {0};
	GetLocalTime(&st);
	message += "-" + to_string(st.wSecond * 1000 + st.wMilliseconds); */
	char msg[1024];
	strcpy_s(msg, message.c_str());
	if (self) networkMessage.push(msg);
	// BoardingMessage变量需要加锁
	if (board) BoardingMessage.push(msg);
	readMtx.unlock();
}

void NetworkSystem::SummonObject(int x, int y, int direction, OperatorName type)
{
	const int SummonDelay = 60;
	GetInstance()->AppendNetworkMessage("SUMMON " +
		to_string(PlayerSystem::GetInstance()->ACKid) + " " +
		to_string(WindowSystem::GetInstance()->GetBattleFrameCnt() + SummonDelay) + " " +
		to_string(x) + " " +
		to_string(y) + " " +
		to_string(direction) + " " +
		to_string(int(type)) + " ",true);
}


void NetworkSystem::Run()
{
	while (true)
	{
		// 取出所有网络消息
		string message = PopNetworkMessage();
		// 如果是空消息则返回
		if (message == "") return;
		// 发送网络消息
		for (auto it = NetworkObjects.begin(); it != NetworkObjects.end(); ++it)
		{
			(*it)->GetNetworkMessage(message);
		}
	}
}

bool NetworkSystem::Init()
{
	//初始化socket环境
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;
	wVersionRequested = MAKEWORD(2, 2);
	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0)
	{
		return -1;
	}
	if (LOBYTE(wsaData.wVersion) != 2 ||
		HIBYTE(wsaData.wVersion) != 2)
	{
		// 清除socket环境
		WSACleanup();
		return -1;
	}
	hSock = socket(AF_INET, SOCK_STREAM, 0);
	//绑定端口
	SOCKADDR_IN servAdr;
	memset(&servAdr, 0, sizeof(servAdr));
	servAdr.sin_family = AF_INET;
	servAdr.sin_port = htons(9999); // 端口号
	inet_pton(AF_INET, "43.136.100.219", &servAdr.sin_addr); // 公网IP地址
	
	//连接服务器
	if (connect(hSock, (SOCKADDR*)&servAdr, sizeof(servAdr)) == SOCKET_ERROR)
	{
		printf("connect error : %d", GetLastError());
		printf("\n");
		return -1;
	}
	//循环收消息
	thread RecvHand(RecvMsg);
	RecvHand.detach();
	// 协议debug用，手动发送消息
	
	// 循环捕捉要发送的消息
	thread SendHand(SendMsg);
	SendHand.detach();
	// 协议debug用
	/*while (1)
	{
		printf("协议DEBUG模式开启，请输入协议内容：\n");
		char msg[1024];
		memset(msg, 0, sizeof(msg));
		fgets(msg, 100, stdin);
		// -1是为了忽略换行符
		send(hSock, msg, strlen(msg)-1, 0);
	}*/
	return true;
}

void NetworkSystem::PreInit()
{
	// 注册回调函数
	MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
		NetworkObject* obj = dynamic_cast<NetworkObject*>(newObj);
		if (obj) NetworkObjects.insert(obj);
		});
	MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
		NetworkObject* obj = dynamic_cast<NetworkObject*>(newObj);
		if (obj) NetworkObjects.erase(obj);
		});

}

NetworkSystem::~NetworkSystem()
{
	WSACleanup();
	closesocket(hSock);
}

