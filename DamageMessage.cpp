﻿#include "DamageMessage.h"

DamageMessage::DamageMessage(int physical, int magic):
	physicalDamage(physical), magicDamage(magic)
{
	// 配置通用情况
	commonModifierCallback= [this](BattleObject* obj) {
		DamageMessage* result = new DamageMessage(*this);
		return result;
	};
	damageModifierCallback = [this](BattleObject* obj) {
		return commonModifierCallback(obj);
	};
}
