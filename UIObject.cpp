﻿#include "UIObject.h"


int UIObject::onUIUpdate()
{
	// 判断点击
	if ( ButtonArea!=nullptr && type!=Destroyed)
	{
		// 获取自身边界
		auto Rect = ButtonArea->getBoundingBox();
		// 判断点是否在边界内部
		if (Rect.containsPoint(Input::getMousePos()))
		{
			// 事件不同时进行，否则可能出错
			if (Input::isPress(MouseCode::Left) )
			{
				if (OnClick) OnClick();
				// 执行附加的函数
				for (auto& callback : appendClickCallbacks) {
					callback();
				}
				return 1;
			}
			else
			{
				// 判断鼠标是否在框内
				if (!OnMouseIn) OnMouseIn = []() {Window::setCursor(Window::Cursor::Hand); };
				OnMouseIn();
				// 执行附加的函数
				for (auto& callback : appendMouseInCallbacks) {
					callback();
				}
				return 1;
			}
		}
		
	}
	return 0;
}

void UIObject::OnGetUIMessage(UIBroadMessage message)
{
}

void UIObject::SetClickAppendCallback(function<void()> append)
{
	if (append == nullptr) appendClickCallbacks.clear();
	else appendClickCallbacks.push_back(append);
}

void UIObject::SetMouseInAppendCallback(function<void()> append)
{
	if (append == nullptr) appendMouseInCallbacks.clear();
	else appendMouseInCallbacks.push_back(append);
}
