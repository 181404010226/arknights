﻿#pragma once
#include "UIObject.h"
#include "easy2d/easy2d.h"

class 侧边栏 : public UIObject
{
private:
    easy2d::Sprite* BackgroudPicture;
    easy2d::Sprite* HeadPicture;
    easy2d::Sprite* DetialPicture;
    float TargetPos;
    // 设定展示的宽度
    const float outWidth = -150.0f;
    const float speed = 300.0f;
public:
    void onCreate();
    void onFrameUpdate();
    /// <summary>
    /// 设置侧边栏要显示的路径
    /// </summary>
    void SetPicturePath(string picturePath);
    void SliderOut();
    void SliderIn();
};