﻿#include "幽灵鲨.h"
#include "SceneConfig.h"

void 幽灵鲨::onCreate()
{
	// 记得调用基类的创建函数(显示血条)
	BattleObject::onCreate();
	// 设置显示层级
	type = playerUnit1;
	IdlePicture[0] = "Image/幽灵鲨/待机/幽灵鲨背面1.png";
	IdlePicture[1] = "Image/幽灵鲨/待机/幽灵鲨向右1.png";
	IdlePicture[2] = "Image/幽灵鲨/待机/幽灵鲨向左1.png";
	IdlePicture[3] = "Image/幽灵鲨/待机/幽灵鲨向右1.png";
	AttackPicture[0] = "Image/幽灵鲨/攻击/幽灵鲨背面攻击1.png";
	AttackPicture[1] = "Image/幽灵鲨/攻击/幽灵鲨向下攻击1.png";
	AttackPicture[2] = "Image/幽灵鲨/攻击/幽灵鲨向左攻击1.png";
	AttackPicture[3] = "Image/幽灵鲨/攻击/幽灵鲨向右攻击1.png";
	unitStamp = gcnew 网格单位贴图(IdlePicture[3]);
	this->addChild(unitStamp);
	// 设置攻击属性
	attack = new AttackMessage(NormalAttack,AllEnemy,72,{0,2});
	attack->rangeRequest = SamePlatform;
	// 设置伤害延迟,与动画匹配（同时也是攻击前摇）
	attack->AttackDelay = 28;
	// 设置伤害与防御属性
	damage = new DamageMessage(335, 0);
	defense = new DefenceMessage(133, 0);
	state = new StateMessage();
	// 设置干员名
	typeName = OperatorName::幽灵鲨;
	// 设置类型为地面类型
	gridType = Groud;
	// 设置生命
	Maxlife = 1311;
	Life = 1311;

}

void 幽灵鲨::onFrameUpdate()
{
	
}

幽灵鲨::~幽灵鲨()
{
	cout << "幽灵鲨清除"  << endl;
}
