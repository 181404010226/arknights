﻿#include "文本矩形.h"

文本矩形::文本矩形(Size size)
{
	Init(size);
}

文本矩形::文本矩形()
{
	// 仅用于系统管理
}

void 文本矩形::onCreate()
{
	Init(getSize());
}

void 文本矩形::SetPicture(string picturePath)
{
	sprite->open(picturePath);
	sprite->setSize(getSize());
}

void 文本矩形::Init(Size size)
{
	// 使得描点变为Size的中心点
	this->setPos(size.width / 2, size.height / 2);
	this->setSize(size);
	this->setAnchor(0.5, 0.5);
	// 生成灰色按钮
	button = ShapeNode::createRect(size);
	button->setFillColor(Color::Gray);
	button->setOrder(10);
	this->addChild(button);
	// 生成图片的容器
	sprite = gcnew Sprite;
	sprite->setSize(getSize());
	sprite->setOrder(20);
	this->addChild(sprite);
	// 设置文本
	text = gcnew Text("");
	text->setFontSize(30);
	text->setPos(button->getSize().width / 2, button->getSize().height / 2);
	text->setAnchor(0.5, 0.5); 
	text->setOrder(30);
	this->addChild(text);
	// 设置默认响应区域
	ButtonArea = button;
	// 设置类型
	type = UI;
}
