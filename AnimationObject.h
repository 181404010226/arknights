﻿#pragma once
#include "MonoObject.h"
#include "网格单位贴图.h"
#include "enum.h"

class AnimationObject : virtual public MonoObject
{
public:
    /// <summary>
    /// 动画帧更新
    /// </summary>
    void onAnimationUpdate();
    /// <summary>
    /// 设置预览方向，并传入单位在第几行
    /// </summary>
    void SetPreViewDirection(int number,int row);
    /// <summary>
    /// 重置动画状态，并设置是否在播放完毕之后自动进入等待
    /// </summary>
    void ResetToState(UnitAnimationState state,bool toIdle=false);
protected:
    网格单位贴图* unitStamp;
    /// <summary>
    ///  (上下左右)网格单位四个朝向的贴图路径
    /// </summary>
    string IdlePicture[4];
    /// <summary>
    /// 四个朝向的攻击动画
    /// </summary>
    string AttackPicture[4];
private:
    bool AutoToIdle = false;
    bool beginPlay = false;
    // 当前播放帧
    int playingFrame = 1;
    // 当前朝向
    int direction = 3;
    // 当前播放string
    string nowPlay;
};

