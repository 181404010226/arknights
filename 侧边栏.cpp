﻿#include "侧边栏.h"
#include "SceneConfig.h"

void 侧边栏::onCreate()
{
    BackgroudPicture = gcnew Sprite();
    HeadPicture = gcnew Sprite();
    DetialPicture = gcnew Sprite();

    BackgroudPicture->setOpacity(0.5);
    BackgroudPicture->setPos(0, 100);
    DetialPicture->setPos(0, 300);

    addChild(BackgroudPicture);
    addChild(HeadPicture);
    addChild(DetialPicture);

    TargetPos = outWidth;
}

void 侧边栏::onFrameUpdate()
{
    // 修改位置
    const float eqs = 1;
    if (TargetPos<getPosX()-1)
        setPos(getPosX()- speed/Sceneconfig::GetInstance()->MaxFrame, 0);
    if (TargetPos > getPosX() + 1)
        setPos(getPosX() + speed / Sceneconfig::GetInstance()->MaxFrame, 0);
    if (getPosX() < outWidth + 10) setOpacity(0);
    else setOpacity(1);
}

void 侧边栏::SetPicturePath(string picturePath)
{
    BackgroudPicture->open(picturePath+"/侧边栏.png");
    HeadPicture->open(picturePath + "/技能.png");
    DetialPicture->open(picturePath + "/属性.png");
    BackgroudPicture->setSize(150, 400);
    DetialPicture->setSize(100, 200);
}

void 侧边栏::SliderOut()
{
    TargetPos = 0;
}

void 侧边栏::SliderIn()
{
    TargetPos = outWidth;
}

