﻿#include "BattleObject.h"
#include "GridObject.h"
#include "MonoSystem.h"
#include "WindowSystem.h"
#include "AnimationObject.h"

void BattleObject::onBattleUpdate()
{
	// 更新血条长度(设置size没有用，因为是shapenode类型)
	BloodBar->button->setScaleX((float)Life/Maxlife);
	// 更新血条颜色
	if (GetComponent<GridObject>(this)->PlayerId == 0) 
		BloodBar->button->setFillColor(Color::Blue);
	else BloodBar->button->setFillColor(Color::Red);
	BloodBar->text->setText(to_string(Life));
	if (NextAttackFrame==-1)
		NextAttackFrame = WindowSystem::GetInstance()->GetBattleFrameCnt() + attack->AttackSpeed-1;
	// 计算战斗
	else if (NextAttackFrame <= WindowSystem::GetInstance()->GetBattleFrameCnt())
	{
		auto objs = attack->GetAttackTargets(this, state);
		if (objs.size() > 0)
		{
			GetComponent<AnimationObject>(this)->ResetToState(UnitAnimationState::Attack,true);
			NextDamageFrame = WindowSystem::GetInstance()->GetBattleFrameCnt() + attack->AttackDelay;
			NextAttackFrame = attack->GetNextAttackFrame(state);
		}
	}
	// 伤害帧判定
	if (NextDamageFrame != -1) MakeDamage();
}

void BattleObject::onCreate()
{
	// 血条黑色背景
	auto rect = ShapeNode::createRect({ (float)LifeBarWidth, (float)LifeBarHeight });
	rect->setFillColor(Color::Black);
	// 模拟血条
	BloodBar = gcnew 文本矩形({ (float)LifeBarWidth, (float)LifeBarHeight });
	BloodBar->setPos(0, -75);
	BloodBar->text->setFontSize(15);
	// 挂载
	this->addChild(BloodBar);
	BloodBar->addChild(rect);
	rect->setOrder(-1);
	// 默认值
	Life = 100;
}

BattleObject::~BattleObject()
{
	if (attack) delete attack;
	if (defense) delete defense;
	if (state) delete state;
	if (damage) delete damage;
}

void BattleObject::MakeDamage()
{
	// 指定帧发动攻击
	if (NextDamageFrame != WindowSystem::GetInstance()->GetBattleFrameCnt()) return;
	auto objs = attack->GetAttackTargets(this, state);
	for (BattleObject* obj : objs)
	{
		// 获取受到状态影响后的攻防
		DamageMessage* tempDamage = damage->damageModifierCallback(this);
		DefenceMessage* tempDefence = obj->defense->defenceModifierCallback(obj);
		// 计算最终伤害
		tempDamage->physicalDamage -=  tempDefence->physicalDefence;
		tempDamage->magicDamage -= (float)tempDamage->magicDamage *((float)tempDefence->magicDefence/100.0f);
		// 执行伤害前回调
		if (damage->damageCalculationCallback)
			damage->damageCalculationCallback(tempDamage, obj);
		if (obj->defense->defenceCalculationCallback)
			obj->defense->defenceCalculationCallback(tempDamage, this);
		// 执行扣血
		obj->Life -= tempDamage->physicalDamage;
		obj->Life -= tempDamage->magicDamage;
		// 释放内存
		delete tempDamage;
		delete tempDefence;
	}
	NextDamageFrame = -1;
}
