﻿#include "网格单位贴图.h"

网格单位贴图::网格单位贴图(string Picture)
{
	// 使得自身描点变为Size的中心点
	this->setPos(0, 0);
	this->setSize(pixel ,pixel);
	// 设置锚点为底边
	this->setAnchor(0.5, 0.75);
	// 初始化
	sprite = gcnew Sprite;
	this->addChild(sprite);
	SwitchPicture(Picture);
}


void 网格单位贴图::SwitchPicture(string picturePath)
{
	// 从本地图片加载
	sprite->open(picturePath);
	// 图片大小
	sprite->setSize(pixel, pixel);
}
