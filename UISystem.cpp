﻿#include "UISystem.h"
#include "MonoSystem.h"


UISystem* UISystem::GetInstance()
{
    // 饿汉式
    static UISystem m_Instance;
    return &m_Instance;
}

void UISystem::Run(bool Pause)
{
    int EventsTriggerCount=0;
    // 执行UI更新
    for (auto it = UIObjects.begin(); it != UIObjects.end(); ++it)
    {
        if (!(*it)->pause)
        {
            if (!Pause) EventsTriggerCount += (*it)->onUIUpdate();
            else if (!(*it)->pauseInGamePause) EventsTriggerCount += (*it)->onUIUpdate();
        }
    }
    if (EventsTriggerCount==0) Window::setCursor(Window::Cursor::Normal);
}

bool UISystem::Init()
{
   
    return true;
}

void UISystem::PreInit()
{ // 注册回调函数
    MonoSystem::GetInstance()->onObjectCreate.push_back([this](MonoObject* newObj) {
        UIObject* obj = dynamic_cast<UIObject*>(newObj);
        if (obj) UIObjects.insert(obj);
        });
    MonoSystem::GetInstance()->onObjectDestroy.push_back([this](MonoObject* newObj) {
        UIObject* obj = dynamic_cast<UIObject*>(newObj);
        if (obj) UIObjects.erase(obj);
        });
}

void UISystem::BroadMessage(UIBroadMessage message)
{
    for (auto it = UIObjects.begin(); it != UIObjects.end(); ++it)
    {
        (*it)->OnGetUIMessage(message);
    }
}

UISystem::~UISystem()
{
}